# Лабораторна робота №8. Основи введення/виведення Java SE∗

### Мета: Оволодіння навичками управління введенням/виведенням даних з використанням класів платформи Java SE.

## 1. Вимоги

### 1.1 Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- 15-12-2021.

### 1.2 Загальне завдання

- Забезпечити можливість збереження і відновлення масива об'єктів рішення завдання лабораторної роботи №7.
- Продемонструвати використання моделі Long Term Persistence.
- Забезпечити діалог з користувачем у вигляді простого текстового меню.
- При збереженні та відновленні даних забезпечити діалоговий режим вибору директорії з відображенням вмісту і можливістю переміщення по підкаталогах.

### 1.3 Індивідуальне завдання:

Виконати загальне завдання згідно до вимог.

## 2. Опис програми

### 2.1 Функціональне призначення

- Створення потоку вводу/виводу об'єкта прикладної галузі.

### 2.2 Засоби ООП

* Було використано геттери та сеттери для полів:

- public int getDuration()
- public int setDuration()

### 2.2 Важливі фрагменти

### Класс Main

* Основний метод:

```
public class Main {
    /**
     * Точка входа, главный метод.
     *
     * @param args - аргументы главного метода
     * @throws IOException    - при неудачной
     *                        работе с файлами
     * @throws ParseException - при неудачном
     *                        парса
     */
    public static void main(final String[] args)
            throws IOException, ParseException {
        boolean endprog = false;
        Scanner inInt = new Scanner(System.in);
        Scanner inStr = new Scanner(System.in);
        int menu;

        EventScheduler[] events = new EventScheduler[0];
        Scanner in = new Scanner(System.in);
        String choice;
        do {
            UI.mainMenu();
            choice = UI.getChoice();
            switch (choice) {
                case "1":
                    events = UI.addEvent(events);
                    break;
                case "2":
                    System.out.print("Введите индекс удаляемого мероприятия: ");
                    int index = in.nextInt();
                    events = UI.dropEvent(events, index);
                    break;
                case "3":
                    UI.printInfo(events);
                    System.out.println();
                    break;
                case "4":
                    UI.saveToFile(events);
                    //UI.FolderMenu(inInt, inStr, events);
                    break;
                case "5":
                    try {
                        EventScheduler[] newEvents = UI.loadFromFile();
                        UI.printInfo(newEvents);
                    } catch (IOException e) {
                        System.out.println(e.toString());
                    }
                    break;
                case "0":
                    System.out.println("Выход...");
                    break;
                default:
                    System.out.println("Введите номер одного из пунктов!\n");
            }
        } while (!choice.equals("0"));
    }
}
```

### Класс EventScheduler

```
public final class UI {
    /**
     * Для чтения данных с клавиатуры.
     */
    private static BufferedReader buffer = new BufferedReader(
            new InputStreamReader(System.in));

    private UI() {
    }

    /**
     * Главное меню.
     */
    public static void mainMenu() {
        System.out.println("1. Добавление мероприятия.");
        System.out.println("2. Удаление мероприятия.");
        System.out.println("3. Вывод информации.");
        System.out.println("4. Сохранить в файл.");
        System.out.println("5. Загрузить из файла.");
        System.out.println("0. Выход.");
        System.out.print("Введите ваш ответ сюда: ");
    }

    /**
     * Получение ответа диалогового меню.
     *
     * @return ответ
     * @throws IOException при неудачном считывании
     */
    public static String getChoice() throws IOException {
        return buffer.readLine();
    }

    /**
     * Добавление мероприятия.
     *
     * @param events - исходный набор мероприятий
     * @return новый набор мероприятий
     * @throws IOException    при неудачном считывании
     * @throws ParseException при неудачном парсинге
     */
    public static EventScheduler[] addEvent(final EventScheduler[] events)
            throws IOException, ParseException {
        EventScheduler[] newEvents = new EventScheduler[events.length + 1];
        System.arraycopy(events, 0, newEvents, 0, events.length);
        newEvents[events.length] = EventScheduler.generate();
        return newEvents;
    }

    /**
     * Удаление мероприятия.
     *
     * @param events - исходный набор мероприятий
     * @param pos    - позиция удаляемого мероприятия
     * @return новый набор мероприятий
     */
    public static EventScheduler[] dropEvent(final EventScheduler[] events,
                                             final int pos) {
        if (pos >= events.length) {
            System.out.println("Error. Out of bounds.");
            return events;
        } else {
            EventScheduler[] newEvents = new EventScheduler[events.length - 1];
            int i = 0;
            for (int j = 0; j < events.length; j++) {
                if (j == pos) {
                    continue;
                } else {
                    newEvents[i] = events[j];
                    i++;
                }
            }
            return newEvents;
        }
    }

    /**
     * Вывод информации о мероприятиях.
     *
     * @param events - набор мероприятий
     */
    public static void printInfo(EventScheduler[] events) {
        for (int i = 0; i < events.length; i++) {
            System.out.format("%nМЕРОПРИЯТИЕ %d:%n", i + 1);
            events[i].printInfo();
        }
    }

    /**
     * Сохранение объекта в файл.
     *
     * @param events - набор мероприятий
     * @throws IOException при неудачном
     *                     считывании или записи.
     */
    public static void saveToFile(final EventScheduler[] events) throws IOException {
        final String semi = ";";
        Pattern pattern = Pattern.compile(semi);
        Matcher matcher;
        String path;
        StringBuilder direct = new StringBuilder();
        System.out.print("Введите имя директории: (; чтобы остановить выбор): ");
        while (true) {
            System.out.print(direct.toString());
            path = buffer.readLine();
            direct.append(path).append("\\");
            matcher = pattern.matcher(direct.toString());
            if (matcher.find()) {
                break;
            }
            File directory = new File(direct.toString());
            File[] list = directory.listFiles();
            if (list == null) {
                System.out.println("Неверное имя директории!");
                if (direct.length() != 0) {
                    direct.delete(
                            direct.length() - path.length() - 1,
                            direct.length());
                }
                continue;
            }
            System.out.println("------------------------------");
            for (File it : list) {
                if (it.isDirectory()) {
                    System.out.print(it.getName());
                    System.out.println(" (...)");
                    continue;
                }
                System.out.println(it.getName());
            }
            System.out.println("------------------------------");
        }
        String currentDir = direct.toString();
        currentDir = currentDir.replaceAll(semi, "");
        FileOutputStream fos = new FileOutputStream(
                currentDir + "\\Serialized.txt");
        XMLEncoder xmlEncoder = new XMLEncoder(new BufferedOutputStream(fos));
        xmlEncoder.writeObject(events);
        xmlEncoder.close();
    }

    /**
     * Загрузка объекта из файла.
     *
     * @return массив объектов
     * @throws IOException при неудачном
     *                     считывании или записи.
     */
    public static EventScheduler[] loadFromFile() throws IOException {
        System.out.print("Введите путь к файлу (указывая название файла и формат): ");
        String dirToExtract = buffer.readLine();
        FileInputStream fis = new FileInputStream(dirToExtract);
        XMLDecoder xmlDecoder = new XMLDecoder(new BufferedInputStream(fis));
        EventScheduler[] getEvents = (EventScheduler[]) xmlDecoder.readObject();
        xmlDecoder.close();
        return getEvents;
    }
}
```

### Варіант застосування

На Рисунках 1-4 зображено результати роботи програми.

![](matushkin-oleksii/doc/demo08/assets/output.png)

_Рисунок 1 - результат додавання заходів_

![](matushkin-oleksii/doc/demo08/assets/delete.png)

_Рисунок 2 - результат видалення заходу_

![](matushkin-oleksii/doc/demo08/assets/ser.png)

_Рисунок 3 - результат серіалізації заходів_

![](matushkin-oleksii/doc/demo08/assets/deser.png)

_Рисунок 4 - результат десеріалізації заходів_

## Висновки

Набув навичок використання об'єктно-орієнтованого підходу для розробки об'єкта предметної галузі.
