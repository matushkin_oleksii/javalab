# Лабораторна робота №4. Інтерактивні консольні програми для платформи Java SE

### Мета: Реалізація діалогового режиму роботи з користувачем в консольних програмах мовою Java.

## 1. Вимоги

### 1.1 Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- 10-11-2021.

### 1.2 Загальне завдання

Виконати прикладну задачу.

### 1.3 Індивідуальне завдання:

- Використовуючи програму рішення завдання лабораторної роботи №3, відповідно до прикладної задачі
  забезпечити обробку команд користувача у вигляді текстового меню:

  введення даних;

  перегляд даних;

  виконання обчислень;

  відображення результату;

  завершення програми і т.д.

- Забезпечити обробку параметрів командного рядка для визначення режиму роботи програми:

  Параметр "-h" чи "-help": відображається інформація про автора програми, призначення (
  індивідуальне завдання), детальний опис режимів роботи (пунктів меню та параметрів командного
  рядка);

  Параметр "-d" чи "-debug": в процесі роботи програми відображаються додаткові дані, що полегшують
  налагодження та перевірку працездатності програми: діагностичні повідомлення, проміжні значення
  змінних, значення тимчасових змінних та ін.

## 2. Опис програми

### 2.1 Функціональне призначення

Розділення текста на речення. В кожному реченні виконується пошук та виведення всіх слів
максимальної та всіх слів мінімальної довжини.

### 2.2 Логічна структура проекту

**Основний метод**

``` public static void main(String[] args) ```

**Призначення:** виклик додаткових функцій, інтерактивна взяємодія з користувачем.

**Метод приймає повний текст, введений користувачем та піддає обробці, виокремленню речень для
подальших обчислень.**

``` private static void sentenceFinder(StringBuilder inputtedText) ```

**Опис роботи:** Метод за допомогою циклу проводить пошку заданного розділового знаку. Записує
знайдене речення в окрему строку та видаляє його з початкового тексту.

![](matushkin-oleksii/doc/demo03/assets/sentenceFinder.png)

_Рисунок 1 - опис функції sentenceFinder_

**Метод sentenceFinderDebug, що виконуєтсья при увімкненному режимі відлагодження.**

``` private static void sentenceFinderDebug(StringBuilder inputtedText) ```

**Опис роботи:** Метод аналогічний методу sentenceFinder, але містить більше інформації, що
виводиться користувачеві для полегшенної перевірки працездатності програми.

![](matushkin-oleksii/doc/demo03/assets/sentenceFinderDebug.png)

_Рисунок 2 - опис функції sentenceFinderDebug_

**Метод розокремлює речення на окремі слова для подальшого пошуку найбільшого та найменого з них.**

``` static void wordFinder(String currentSentence) ```

**Опис роботи:** Метод виконує розділення речення на окремі слова і шукає найбільші і найменші з
них. Результат зберігається у масиві строк.

![](matushkin-oleksii/doc/demo03/assets/wordFinder.png)

_Рисунок 3 - опис функції wordFinder_

**Метод wordFinderDebug, що виконуєтсья при увімкненному режимі відлагодження..**

``` static void wordFinderDebug(String currentSentence) ```

**Опис роботи:** Метод аналогічний методу wordFinder, але містить більше інформації, що виводиться
користувачеві для полегшенної перевірки працездатності програми.

![](matushkin-oleksii/doc/demo03/assets/wordFinderDebug.png)

_Рисунок 4 - опис функції wordFinderDebug_

**Метод форматування результата**

``` private static void outputFormatter(int parameter, String[] words, int count) ```

**Опис роботи:** Метод виконує форматування тексту результата та зберігає його в пам'яті до момента,
коли користувач дасть команду виведення.

![](matushkin-oleksii/doc/demo03/assets/outputFormatter.png)

_Рисунок 5 - опис функції outputFormatter_

**Метод виведення поточних даних**

``` private static StringBuilder currentData(StringBuilder inputtedText, boolean debugMode) ```

**Опис роботи:** Метод виводить поточні дані: текст, що було введенно та стан режиму відлагодження.

![](matushkin-oleksii/doc/demo03/assets/currentData.png)

_Рисунок 6 - опис функції currentData_

**Метод виведення результата обробки**

``` public static void showResult(List<String> Container) ```

**Опис роботи:** Метод виводить результат роботи програми.

![](matushkin-oleksii/doc/demo03/assets/showResult.png)

_Рисунок 7 - опис функції showResult_

**Метод контролює увімкнення та вимкнення режиму відлагодження.**

``` private static boolean debugModeControl(boolean debugMode) ```

**Опис роботи:** Метод контролює режим відлагодження.

![](matushkin-oleksii/doc/demo03/assets/debugModeControl.png)

_Рисунок 8 - опис функції debugModeControl_

**Метод, що виводить додаткову інформацію**

``` private static void flagHelp() ```

**Опис роботи:** Метод виводить інформацію про автора та призначення флагів.

![](matushkin-oleksii/doc/demo03/assets/flagHelp.png)

_Рисунок 9 - опис функції flagHelp_

### 2.2 Важливі фрагменти функції

Код функції currentData:

```
private static StringBuilder currentData(StringBuilder inputtedText, boolean debugMode) {
    Scanner in = new Scanner(System.in);
    if (inputtedText.length() == 0) {
        System.out.println("You did not enter text");
        System.out.println("Want to do it now?\n1) Yes\n2) No");
        int answer = in.nextInt();
        if (answer == 1) {
            System.out.println("Enter text:");
            in.nextLine();
            inputtedText = new StringBuilder(in.nextLine());
            System.out.println("Inputted text: " + inputtedText);
            return inputtedText;
        }
    } else {
        System.out.println("Current text: " + inputtedText);
    }
    System.out.println("Debug mode: " + debugMode);
    return inputtedText;
}
```

Код функції outputFormatter:

```
private static void outputFormatter(int parameter, String[] words, int count, List<String> Container) {
Formatter f = new Formatter(); //java.util class
if (parameter == 1) {
    f.format("The smallest words are: ");
    for (int i = 0; i < count; i++) {
        if (i == 0)
            f.format(words[i]);
        else
            f.format(", " + words[i]);
    }
    f.format("\t\t");
} else if (parameter == 2) {
    f.format("The largest words are: ");
    for (int i = 0; i < count; i++) {
        if (i == 0)
            f.format(words[i]);
               else
               f.format(", " + words[i]);
        }
        f.format("\t\t");
    }
String string = f.toString();
Container.add(string);
}
```

Код функції wordFinderDebug:

```
static void wordFinderDebug(String currentSentence, List<String> Container) {
   System.out.println("\n**Выполнение метода wordFinder**");
   StringBuilder word = new StringBuilder();
   String[] allLargeWords = new String[10]; //Массив наибольших слов
   String[] allSmallWords = new String[10]; //Массив наименьших слов
   String smallWord;
   String largeWord;

   String[] allWords = new String[50]; //Массив слов, размер задан для вмещения 50-ти слов
   int ammounOfWords = 0; //Количество слов в массиве allWords[]

   currentSentence += " "; //В конец пробел, чтобы можно было определить последнее слово
   System.out.println("\nОбработка предложения: " + currentSentence);

   for (int i = 0; i < currentSentence.length(); i++) { //Перебор предложения в поиске отдельных слов
            char c = currentSentence.charAt(i);
            if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57) || c == '-' || c == '\'')) {
                word.append(currentSentence.charAt(i)); //Запись отдельного слова для дальнейшего помещения в массив allWords[]
            } else if (word.toString().equals("")) {
            } else {
                allWords[ammounOfWords] = word.toString(); //Добавление найденного слова
                ammounOfWords++; //Добавить 1 к счетчику слов
                word = new StringBuilder(); //Очистить StringBuilder для записи нового слова
            }
        }
        smallWord = largeWord = allWords[0]; //Начальное слово

        for (int k = 0; k < ammounOfWords; k++) //Цикл поиска найбольшего и найментшего слов {
            if (smallWord.length() > allWords[k].length()) //Если тукущее наименьшее слово больше, чем текущее обрабатываемое слово
            {
                smallWord = allWords[k]; //Заменяем наименьшее
            }
            if (largeWord.length() < allWords[k].length()) //Если тукущее наибольшее слово больше, чем текущее обрабатываемое слово
            {
                largeWord = allWords[k]; //Заменяем наибольшее
            }
        }
        int smallCounter = 0;
        int largeCounter = 0;
        for (int k = 0; k < ammounOfWords; k++) //Цикл поиска равных по длинне слов
        {
            if (largeWord.length() == allWords[k].length()) {
                allLargeWords[largeCounter] = allWords[k];
                largeCounter++;
            }

            if (smallWord.length() == allWords[k].length()) {
                allSmallWords[smallCounter] = allWords[k];
                smallCounter++;
            }
        }
        System.out.println("\nРезультат обработки: ");
        outputFormatter(1, allSmallWords, smallCounter, Container);
        outputFormatter(1, allLargeWords, largeCounter, Container);
        Container.add("\n");
    } //Works correctly
```

### Варіант застосування

Розокремлення введенного тексту на речення. Пошук у реченні найбільших та найменших слів.

![](matushkin-oleksii/doc/demo03/assets/result.png)

_Рисунок 10 - резултат роботи програми_

## Висновки

В ході даної лабораторної роботи мною було набуто навичок реалізації діалогового режиму роботи з користувачем в консольних програмах.
