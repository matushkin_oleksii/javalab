# Лабораторна робота №5. Розробка власних контейнерів. Ітератори
### Мета: Набуття навичок розробки власних контейнерів. Використання ітераторів.
## 1. Вимоги

### 1.1  Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- 19-11-2021.

### 1.2 Загальне завдання

Виконати прикладну задачу.

### 1.3 Індивідуальне завдання:

- Розробити клас-контейнер, що ітерується для збереження початкових даних завдання л.р. №3 у вигляді масиву рядків з можливістю додавання, видалення і зміни елементів.
* В контейнері реалізувати та продемонструвати наступні методи:
- String toString() повертає вміст контейнера у вигляді рядка;
- void add(String string) додає вказаний елемент до кінця контейнеру;
- void clear() видаляє всі елементи з контейнеру;
- boolean remove(String string) видаляє перший випадок вказаного елемента з контейнера;
- Object[] toArray() повертає масив, що містить всі елементи у контейнері;
- int size() повертає кількість елементів у контейнері;
- boolean contains(String string) повертає true, якщо контейнер містить вказаний елемент;
- boolean containsAll(Container container) повертає true, якщо контейнер містить всі елементи з зазначеного у параметрах;
- public Iterator<String> iterator() повертає ітератор відповідно до Interface Iterable.
* В класі ітератора відповідно до Interface Iterator реалізувати методи:
- public boolean hasNext(); 
- public String next();
- public void remove().

## 2. Опис програми

### 2.1 Функціональне призначення

Створення власного класа-контейнера, що ітерується для збереження початкових даних завдання л.р. №3 у вигляді масиву рядків.

### 2.2 Логічна структура проекту

**Клас-контейнер**

``` public class aContainer implements Iterable<String> ```

**Призначення:** реалізація методів контейнера.

![](matushkin-oleksii/doc/demo04/assets/aContainer.png)

_Рисунок 1 - опис класа_

**Масив рядків, що зберігає дані**

![](matushkin-oleksii/doc/demo04/assets/values.png)

**Метод повертає вміст контейнера у вигляді рядка.**

``` public String toString() ```

**Призначення:** повертає вміст у вигляді рядка.

![](matushkin-oleksii/doc/demo04/assets/aContainer/toString.png)

_Рисунок 2 - опис метода toString_

**Метод додає вказаний елемент до кінця контейнеру.**

``` public boolean add(String string) ```

**Опис роботи:** додає вказаний елемент до кінця контейнеру, створюючи новий масив збільшенного розміру.

![](matushkin-oleksii/doc/demo04/assets/aContainer/add.png)

_Рисунок 3 - опис метода add_

**Метод очищує контейнер.**

``` public void clear() ```

**Призначення:** Метод очищує контейнер, замінюючи всі значення на нульові.

![](matushkin-oleksii/doc/demo04/assets/aContainer/clear.png)

_Рисунок 4 - опис метода clear_

**Метод видаляє перший випадок вказаного елемента з контейнера.**

``` boolean remove(String string) ```

**Призначення:** Метод видаляє перший випадок вказаного елемента з контейнера.

![](matushkin-oleksii/doc/demo04/assets/aContainer/remove.png)

_Рисунок 5 - опис метода remove_

**Метод повертає масив, що містить всі елементи у контейнері.**

``` public String[] toArray() ```

**Призначення:** Метод конвертує зміст контейнера у масив.

![](matushkin-oleksii/doc/demo04/assets/aContainer/toArray.png)

_Рисунок 6 - опис метода toArray_

**Метод повертає розмір контейнера.**

``` public int size() ```

**Призначення:** Метод повертає розмір контейнера.

![](matushkin-oleksii/doc/demo04/assets/aContainer/size.png)

_Рисунок 7 - опис метода size_

**Метод визначає наявність елемента у контейнері.**

``` boolean contains(String string) ```

**Призначення:** Метод повертає true, якщо контейнер містить вказаний елемент.

![](matushkin-oleksii/doc/demo04/assets/aContainer/contains.png)

_Рисунок 8 - опис метода contains_

**Метод порівнює вміст двох контейнерів.**

``` boolean containsAll(aContainer container) ```

**Призначення:** Метод повертає true, якщо контейнер містить всі елементи з зазначеного у параметрах.

![](matushkin-oleksii/doc/demo04/assets/aContainer/containsAll.png)

_Рисунок 9 - опис метода containsAll_

**Метод створює ітератор для контейнера.**

``` public aIterator iterator() ```

**Призначення:** Метод створює новий ітератор для контейнера.

![](matushkin-oleksii/doc/demo04/assets/aContainer/iterator.png)

_Рисунок 10 - опис метода iterator_

**Клас-ітератор**

```  public class aIterator implements Iterator<String> ```

**Призначення:** реалізація методів ітератора.

![](matushkin-oleksii/doc/demo04/assets/aIterator/aIterator.png)

_Рисунок 11 - опис класа_

**Метод перевірки наступного елемента.**

``` public boolean hasNext() ```

**Призначення:** Метод повертае true, якщи наявний наступний після поточного елемент.

![](matushkin-oleksii/doc/demo04/assets/aContainer/hasNext.png)

_Рисунок 12 - опис метода hasNext_

**Метод повертає елемент контейнера.**

``` public String next() ```

**Призначення:** Метод повертае елемент контейнера, якщо він існує.

![](matushkin-oleksii/doc/demo04/assets/aContainer/next.png)

_Рисунок 13 - опис метода next_

**Метод видаляє елемент контейнера.**

``` public void remove() ```

**Призначення:** Метод видаляє елемент, на який востаннє вказував курсор.

![](matushkin-oleksii/doc/demo04/assets/aContainer/remove.png)

_Рисунок 14 - опис метода remove_

### 2.2 Важливі фрагменти

Код функції contains:

```
    boolean contains(String string) {
        for (String s : values) {
            if (Objects.equals(s, string)) {
                return true;
            }
        }
        return false;
    }
```
Код функції containsAll:

```
    boolean containsAll(aContainer container) {
        if (values == null || values.length == 0) {
            return false;
        }
        int il = 0;
        String[] toCompare;
        toCompare = container.toArray();
        for (int i = 0; i < container.size(); i++) {
            if (this.contains(toCompare[i])) {
                il++;
            }
        }
        return il == container.size();
    }
```

Код функції remove:

```
    boolean remove(String string) {
        int pos = 0;
        for (int i = 0; i < values.length; i++) {
            if (Objects.equals(values[i], string)) {
                break;
            } else pos++;
        }
        try {
            String[] temp = values;
            values = new String[temp.length - 1];
            System.arraycopy(temp, 0, values, 0, pos);
            int elemToDestinate = temp.length - pos - 1; //позиция, в которую нужно начинать копировать кроме вырезанного
            System.arraycopy(temp, pos + 1, values, pos, elemToDestinate);
            return true;
        } catch (ClassCastException ex) {
            System.err.println("TUT ClassCastException");
        }
        return false;
    }
```
Код функції add:
```
    public boolean add(String string) {
        try {
            String[] temp = values;
            values = new String[temp.length + 1];
            System.arraycopy(temp, 0, values, 0, temp.length);
            values[values.length - 1] = string;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

```

### Варіант застосування

Використання у якості контейнера для л/р-№3, що буде містити початкові дані з можливістю їх модифікації.

![](matushkin-oleksii/doc/demo04/assets/result.png)

_Рисунок 15 - резултат роботи програми_

## Висновки

В ході даної лабораторної роботи мною було набуто навичок розробки власних контейнерів, використання ітераторів.
