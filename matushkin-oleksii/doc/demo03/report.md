# Лабораторна робота №3. Утилітарні класи. Обробка масивів і рядків
### Мета: Розробка власних утилітарних класів. Набуття навичок вирішення прикладних задач з використанням масивів і рядків.
## 1. Вимоги

### 1.1  Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- варіант 13

- 25-10-2021.

### 1.2 Загальне завдання

Виконати прикладну задачу згідно з варіантом.

### 1.3 Індивідуальне завдання:

- Ввести текст. Текст розбити на речення. Для кожного речення знайти та надрукувати всі слова максимальної та всі слова мінімальної довжини. Результат вивести у вигляді таблиці.

## 2. Опис програми

### 2.1 Функціональне призначення

Розділення текста на речення. В кожному реченні виконується пошук та виведення всіх слів максимальної та всіх слів мінімальної довжини.

### 2.2 Логічна структура проекту

**Основний метод**

 ``` public static void main(String[] args) ```
 
 **Призначення:** виклик додаткових функцій


**Метод приймає повний текст, введений користувачем та піддає обробці, виокремленню речень для подальших обчислень.**

 ``` private static void sentenceFinder(StringBuilder inputtedText) ```

 **Опис роботи:** Метод за допомогою циклу проводить пошку заданного розділового знаку. Записує знайдене речення в окрему строку та видаляє його з початкового тексту.

![](matushkin-oleksii/doc/demo02/assets/sentenceFinder.png)

_Рисунок 1 - опис функції sentenceFinder_

**Метод розокремлює речення на окремі слова для подальшого пошуку найбільшого та найменого з них.**

 ```  static void wordFinder(String currentSentence) ``` 

 **Опис роботи:** Метод виконує розділення речення на окремі слова і шукає найбільші і найменші з них. Результат зберігається у масиві строк.

 ![](matushkin-oleksii/doc/demo02/assets/wordFinder.png)

 _Рисунок 2 - опис функції wordFinder_

 **Метод форматування та виведення результата**

 ``` private static void outputFormatter(int parameter, String[] words, int count) ``` 

 **Опис роботи:** Метод виконує форматування тексту результата та виводить його у консоль.

 ![](matushkin-oleksii/doc/demo02/assets/outputFormatter.png)
 
 _Рисунок 3 - опис функції outputFormatter_

### 2.2 Важливі фрагменти функції

Код функції sentenceFinder:

```
private static void sentenceFinder(StringBuilder inputtedText){
String currentSentence; //хранение текущего предложения
int lastIterationIndex = 0; //сюда будет записываться индекс найденного символа (!/?/.)
char c;
int currentLength = inputtedText.length();
for(int i = 0; currentLength > 1; i++){
    if (i != 0){
        currentSentence = inputtedText.substring(0, lastIterationIndex); //Запись найденного предложения в отдельную строку для дальнейшей обработки
        inputtedText.delete(0, lastIterationIndex + 2); //Обрезка текста с 0 по знак (!/?/.) т.е. теперь все поле текста будет без найденного
        //ранее предложения. Цикл по его обработке сделаю потом. По идее теперь в inputtedText должен храниться текст без первого предложения.
        //+2 убирает знак и следующий за ним пробел
        System.out.println("\n");
        System.out.println("Current sentence: " + currentSentence);
        wordFinder(currentSentence);
    }
    currentLength = inputtedText.length();
    if (currentLength <= 1){
        break;
    }
    for (int j = 0; j < currentLength; j++){
        c = inputtedText.charAt(j);
        if(c == '!' || c == '.' || c == '?'){
            lastIterationIndex = j;
            i++;
            currentLength = inputtedText.length();
            break;
        }
    }
}
}
```

Код функції findHighestNumber:

```
        static void wordFinder(String currentSentence) {
        StringBuilder word = new StringBuilder();
        String[] allLargeWords = new String[10]; //Массив наибольших слов
        String[] allSmallWords = new String[10]; //Массив наименьших слов
        String smallWord;
        String largeWord;

        String[] allWords = new String[50]; //Массив слов, размер задан для вмещения 50-ти слов
        int ammounOfWords = 0; //Количество слов в массиве allWords[]

        currentSentence += " "; //В конец пробел, чтобы можно было определить последнее слово

        for(int i = 0; i < currentSentence.length(); i++){ //Перебор предложения в поиске отдельных слов
            char c = currentSentence.charAt(i);
            if(((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57) || c == '-' || c == '\'')) {
                word.append(currentSentence.charAt(i)); //Запись отдельного слова для дальнейшего помещения в массив allWords[]
            }
            else if(word.toString().equals("")) {
            }
            else {
                allWords[ammounOfWords] = word.toString(); //Добавление найденного слова
                ammounOfWords++; //Добавить 1 к счетчику слов
                word = new StringBuilder(); //Очистить StringBuilder для записи нового слова
            }
        }
        smallWord = largeWord = allWords[0]; //Начальное слово

        for(int k = 0; k < ammounOfWords; k++){ //Цикл поиска найбольшего и найментшего слов
            if(smallWord.length() > allWords[k].length()){//Если тукущее наименьшее слово больше, чем текущее обрабатываемое слово
                smallWord = allWords[k]; //Заменяем наименьшее
            }
            if(largeWord.length() < allWords[k].length()){ //Если тукущее наибольшее слово больше, чем текущее обрабатываемое слово
                largeWord = allWords[k]; //Заменяем наибольшее
            }
        }
        int smallCounter = 0;
        int largeCounter = 0;
        for(int k = 0; k < ammounOfWords; k++){ //Цикл поиска равных по длинне слов
            if(largeWord.length() == allWords[k].length()){
                allLargeWords[largeCounter] = allWords[k];
                largeCounter++;
            }
            if(smallWord.length() == allWords[k].length()){
                allSmallWords[smallCounter] = allWords[k];
                smallCounter++;
            }
        }
        outputFormatter( 1, allSmallWords, smallCounter);
        outputFormatter( 2, allLargeWords, largeCounter);

    }

```

Код функції outputFormatter:

```
        private static void outputFormatter(int parameter, String[] words, int count){ //Method formats output information
        Formatter f = new Formatter(); //java.util class
        if (parameter == 1) {
            f.format("The smallest words are: ");
            for (int i = 0; i < count; i++) {
                if(i == 0)
                f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        }
        else if (parameter == 2){
            f.format("The largest words are: ");
            for (int i = 0; i < count; i++) {
                if(i == 0)
                    f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        }
        System.out.print(f + "\n");
    }

```

### Варіант застосування

Розокремлення введенного тексту на речення. Пошук у реченні найбільших та найменших слів.

![](matushkin-oleksii/doc/demo02/assets/result.png)

_Рисунок 4 - резултат роботи програми_

## Висновки

В ході даної лабораторної роботи мною було набуто навичок розробки програм для платформи Java SE, вирішення прикладних задач з використанням масивів і рядків
