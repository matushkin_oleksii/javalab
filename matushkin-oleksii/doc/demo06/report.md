# Лабораторна робота №6. Серіалізація/десеріалізація об'єктів. Бібліотека класів користувача
### Мета: Ознайомлення з принципами серіалізації/десеріалізації об'єктів. Використання бібліотек класів користувача.
## 1. Вимоги

### 1.1  Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- 15-12-2021.

### 1.2 Загальне завдання

Виконати прикладну задачу.

### 1.3 Індивідуальне завдання:

- Реалізувати і продемонструвати тривале зберігання/відновлення раніше розробленого контейнера за допомогою серіалізації/десеріалізації.
- Обмінятися відкомпільованим (без початкового коду) службовим класом (Utility Class) рішення задачі л.р. №3 з іншим студентом (визначає викладач).
- Продемонструвати послідовну та вибіркову обробку елементів розробленого контейнера за допомогою власного і отриманого за обміном службового класу.
- Реалізувати та продемонструвати порівняння, сортування та пошук елементів у контейнері.
- Розробити консольну програму та забезпечити діалоговий режим роботи з користувачем для демонстрації та тестування рішення.

## 2. Опис програми

### 2.1 Функціональне призначення

Створення власного класа-контейнера, що ітерується для збереження початкових даних завдання л.р. №3 у вигляді масиву рядків.

### 2.2 Засоби ООП
Було додано нові методи для роботи з класом-контейнером:
- сортування елементів: void alphabetSorting(int sort).
- виведення всіх елементів у консоль: public void output().
- пошук елемента:  int search(String str).

### 2.2 Важливі фрагменти
### Класс Main

Основний метод:

```
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        AContainerMod container = new AContainerMod();
        boolean continueWorking = true;
        Scanner stringIn = new Scanner(System.in);
        Scanner integerIn = new Scanner(System.in);
        while (continueWorking) {
            System.out.println("Main menu:");
            System.out.println("1. New data");
            System.out.println("2. Show data");
            System.out.println("3. Add element");
            System.out.println("4. Delete element");
            System.out.println("5. Clear container");
            System.out.println("6. Sort container");
            System.out.println("7. Find element");
            System.out.println("8. Compare elements");
            System.out.println("9. Someones class");
            System.out.println("10. My class");
            System.out.println("11. Serialize container");
            System.out.println("12. Deserialize");
            System.out.println("13. Exit");
            System.out.println("Enter option:");
            int option = integerIn.nextInt();
            System.out.println();
            switch (option) {
                case 1 -> {
                    container.clear();
                    System.out.println("Amount of sentences to add");
                    int sentenceAmount = integerIn.nextInt();
                    String[] toAdd = new String[sentenceAmount];
                    System.out.println("Enter sentences. [Confirm by ENTER]:");
                    for (int i = 0; i < sentenceAmount; i++) toAdd[i] = stringIn.nextLine();
                    container = new AContainerMod(toAdd);
                }
                case 2 -> container.output();
                case 3 -> {
                    System.out.println("Enter data:");
                    container.add(stringIn.nextLine());
                    System.out.println();
                    container.output();
                }
                case 4 -> {
                    System.out.println("Element to delete:");
                    container.remove(stringIn.nextLine());
                    container.remove(stringIn.nextLine());
                }
                case 5 -> {
                    container.clear();
                    System.out.println("Cleared! \n");
                }
                case 6 -> {
                    System.out.println("Ascending order [1]");
                    System.out.println("Descending order [2]");
                    int choice = integerIn.nextInt();
                    if (choice == 1 || choice == 2) {
                        container.alphabetSorting(choice);
                    } else
                        System.out.println("Wrong operation");
                }
                case 7 -> {
                    System.out.println("Element to search:");
                    int position = container.search(stringIn.nextLine());
                    if (position != -1)
                        System.out.println("Your choice: " + (position + 1) + "\n");
                    else
                        System.out.println("There is no such element\n");
                }
                case 8 -> {
                    System.out.println("Your container: ");
                    container.output();
                    System.out.println("Enter positions of elements (from 1 to " + container.size() + "):");
                    int position1 = integerIn.nextInt();
                    int position2 = integerIn.nextInt();
                    boolean comp = container.compareElements(position1, position2);
                    if (comp)
                        System.out.println("Elements [" + position1 + "] and [" + position2 + "] are equal\n");
                    else
                        System.out.println("Elements [" + position1 + "] and [" + position2 + "] are not equal\n");
                }
                case 9 -> {
                    String text = SameFirstLast.getText();
                    SameFirstLast.findWords(text);
                }
                case 10 -> {
                    StringBuilder sentences = new StringBuilder(container.toString());
                    TextProcessor.sentenceFinder(sentences);
                }
                case 11 -> {
                    System.out.println("Serialization...");
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("S:\\University\\JavaTasksKhPI\\javalab"
                            + "\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin06\\SerializeFile.txt"));
                    oos.writeObject(container);
                    oos.close();
                    System.out.println("Serialization completed\n");
                }
                case 12 -> {
                    System.out.println("Deserialization...");
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("S:\\University\\JavaTasksKhPI\\javalab"
                            + "\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin06\\SerializeFile.txt"));
                    AContainerMod temp = (AContainerMod) ois.readObject();
                    ois.close();
                    System.out.println("Deserialization completed\n");
                    temp.output();
                }
                case 13 -> {
                    container.clear();
                    integerIn.close();
                    stringIn.close();
                    continueWorking = false;
                }
                default -> System.out.println("Choose right variant\n");
            }
        }
    }

}
```

###Клас AContainerMod:
```
public class AContainerMod implements Iterable<String>, Serializable {

    /**
     * Holds the elements of a container.
     */
    public String[] values;
    int size = 0;

    /**
     * Constructor for making new string
     */
    public AContainerMod(String... toAdd) {
        if (toAdd.length != 0) {
            size = toAdd.length;
            values = new String[size];
            for (int i = 0; i < size; i++)
                values[i] = toAdd[i];
        }
    }

    /**
     * Method concatenates all container elements into a string.
     *
     * @return container in a string
     */
    public String toString() {
        StringBuilder string = new StringBuilder(new String());
        for (String s : values)
            string.append(s + " ");
        return string.toString();
    }

    /**
     * Method for adding elements to a container.
     *
     * @param string - string to initialize a new container element
     */
    public boolean add(String string) {
        try {
            String[] temp = values;
            values = new String[temp.length + 1];
            System.arraycopy(temp, 0, values, 0, temp.length);
            values[values.length - 1] = string;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * Method for resetting a container.
     */
    public void clear() {
        values = new String[0];
    }

    /**
     * Method for removing an exact element by string criteria.
     *
     * @param string string to specify the element to remove
     * @return false if removing cannot be done(no elements in container)
     * true if element has been found and successfully deleted
     */
    boolean remove(String string) {
        boolean smth = false;
        int pos = 0;
        for (String value : values) {
            if (Objects.equals(value, string)) {
                smth = true;
                break;
            } else pos++;
        }
        if (smth) {
            try {
                String[] temp = values;
                values = new String[temp.length - 1];
                System.arraycopy(temp, 0, values, 0, pos);
                int elemToDestinate = temp.length - pos - 1; //позиция, в которую нужно начинать копировать кроме вырезанного
                System.arraycopy(temp, pos + 1, values, pos, elemToDestinate);
                return true;
            } catch (ClassCastException ex) {
                System.err.println("TUT ClassCastException");
            }
        }
        return false;
    }

    /**
     * Method for converting container to an array.
     *
     * @return an array of container elements
     */
    public String[] toArray() {
        return Arrays.copyOf(values, values.length);
    }

    /**
     * Method for receiving the size of container.
     *
     * @return current container size
     */
    public int size() {
        int a = 0;
        try {
            a = values.length;
        } catch (NullPointerException e) {
            System.err.println("No elements");
        }
        return a;
    }

    /**
     * Method for checking a container elements with a specified string.
     *
     * @param string string to search in a container
     * @return true if contains, false if does not contain
     */
    boolean contains(String string) {
        for (String s : values) {
            if (Objects.equals(s, string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for checking the equality of two containers.
     *
     * @param container for comparing with another container
     * @return true if both containers are the same
     * false if they are different
     */
    boolean containsAll(AContainerMod container) {
        if (values == null || values.length == 0) {
            return false;
        }
        int size = 0;
        String[] toCompare;
        toCompare = container.toArray();
        for (int i = 0; i < container.size(); i++) {
            if (this.contains(toCompare[i])) {
                size++;
            }
        }
        return size == container.size();
    }

    /**
     * Method outputs container
     */
    public void output() {
        if (size() > 0) {
            for (String str : values)
                System.out.println(str);
        } else {
            System.out.println("Container is empty");
        }
        System.out.println();
    }

    /**
     * Method finds and element in container
     *
     * @param str string that should be founded
     * @return position / -1 - if no such element
     */
    public int search(String str) {
        int position = 0;
        for (String string : values) {
            if (string.equals(str))
                return position;
            position++;
        }
        return -1;
    }

    /**
     * Method sorts container
     *
     * @param sort type of sorting: ascending or descending
     */
    public void alphabetSorting(int sort) {
        if (sort == 1) {
            for (int i = 0; i < size - 1; i++)
                for (int j = 0; j < size - 1; j++)
                    if (values[j].compareTo(values[j + 1]) > 0) {
                        String temp = values[j];
                        values[j] = values[j + 1];
                        values[j + 1] = temp;
                    }
        } else if (sort == 2) {
            for (int i = 0; i < size - 1; i++)
                for (int j = 0; j < size - 1; j++)
                    if (values[j].compareTo(values[j + 1]) < 0) {
                        String temp = values[j];
                        values[j] = values[j + 1];
                        values[j + 1] = temp;
                    }
        }
    }

    /**
     * Method compares two elements
     *
     * @param position1 first element's position
     * @param position2 second element's position
     * @return -1 - wrong positions / 1 - strings are equal / 0 - strings are not equal
     */
    public boolean compareElements(int position1, int position2) {
        if (position1 > size || position2 > size) {
            if (position1 < 0 || position2 < 0)
                return false;
        }
        if (values[position1 - 1].equals(values[position2 - 1]))
            return true;
        else
            return false;
    }

    /**
     * Method for creating a correct iterator.
     *
     * @return a new iterator to a Container object
     */
    public AIterator iterator() {
        return new AIterator(values);
    }

    /**
     * Class AIterator.
     * Contains two fields of lower and higher bound of a container.
     * Constructor gets a storage field from Container and defines
     * both bounds.
     * Contains methods for iterating over a container,
     * checking the existence of the next element and removing.
     *
     * @author Matushkin Oleksii
     */
    public class AIterator implements Iterator<String> {
        /**
         * Lower bound of the container
         */
        private static int firstBound;

        /**
         * Upper bound of the container
         */
        private static int lastBound;

        /**
         * Constructor for processing the container data.
         * Defines values of lower and higher bound.
         *
         * @param values - array of container elements
         */
        public AIterator(String[] values) {
            firstBound = -1;
            lastBound = values.length - 1;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return firstBound < lastBound;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public String next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            } else {
                firstBound++;
                return values[firstBound];
            }
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).  This method can be called
         * only once per call to {@link #next}.
         */
        @Override
        public void remove() {
            try {
                String[] temp = values;
                values = new String[temp.length - 1];
                System.arraycopy(temp, 0, values, 0, firstBound);
                int elemToDestinate = temp.length - firstBound - 1; //позиция, в которую нужно начинать копировать кроме вырезанного
                System.arraycopy(temp, firstBound + 1, values, firstBound, elemToDestinate);
            } catch (ClassCastException ex) {
                System.err.println("tut hueta");
            }
        }
    }

}

```


### Варіант застосування

Результати виконання методів зображено на Рисунках 1-7.

![](matushkin-oleksii/doc/demo06/assets/enter_data.png)

_Рисунок 1 - результат створення контейнера з вмістом_

![](matushkin-oleksii/doc/demo06/assets/delete.png)

_Рисунок 2 - результат видалення введенної строки_

![](matushkin-oleksii/doc/demo06/assets/search.png)

_Рисунок 3 - результат пошуку заданної строки_

![](matushkin-oleksii/doc/demo06/assets/someones_class.png)

_Рисунок 4 - результат виконання утилітарного класу одногрупника_

![](matushkin-oleksii/doc/demo06/assets/sort.png)

_Рисунок 5 - результат сортування вмісту контейнера_

![](matushkin-oleksii/doc/demo06/assets/serializ.png)

_Рисунок 6 - результат серіалізації контейнера_

![](matushkin-oleksii/doc/demo06/assets/deser.png)

_Рисунок 7 - результат десеріалізації контейнера_

## Висновки

В ході даної лабораторної роботи мною було набуто навичок тривалого зберігання та відновлення стану об'єктів. Ознайомився з принципами серіалізації/десеріалізації об'єктів та використання
бібліотек класів користувача.
