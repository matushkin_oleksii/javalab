# Лабораторна робота №7. Використання об'єктно-орієнтованого підходу для розробки об'єкта предметної (прикладної) галузі

### Мета: Ознайомлення з принципами серіалізації/десеріалізації об'єктів. Використання бібліотек класів користувача.

## 1. Вимоги

### 1.1 Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- 14 варіант

- 15-12-2021.

### 1.2 Загальне завдання

- Використовуючи об'єктно-орієнтований аналіз, реалізувати класи для представлення сутностей
  відповідно прикладної задачі - domainоб'єктів.
- Забезпечити та продемонструвати коректне введення та відображення кирилиці.
- Продемонструвати можливість управління масивом domain-об'єктів.

### 1.3 Індивідуальне завдання:

- 14 Планувальник
- Захід: дата, час початку і тривалість; місце проведення; опис; учасники (кількість не обмежена).

## 2. Опис програми

### 2.1 Функціональне призначення

Створення власного класа, згідно теми прикладної області. у вигляді масиву рядків.

### 2.2 Засоби ООП

* Було використано геттери та сеттери для полів:

- public int getDuration()
- public int setDuration()

### 2.2 Важливі фрагменти

### Класс Main

* Основний метод:

```
public class Main {
        public static void main(final String[] args)
            throws IOException, ParseException {
            Scanner in = new Scanner(System.in);
            System.out.print("Сколько мероприятий добавить? ");
            int size = in.nextInt();
            in.nextLine();
            EventScheduler[] events = new EventScheduler[size];
            for (int i = 0; i < events.length; i++) {
                System.out.format("Мероприятие %d:%n", i + 1);
                events[i] = EventScheduler.generate();
            }
            System.out.println();
            for (int i = 0; i < events.length; i++) {
                System.out.format("Мероприятие %d:%n", i + 1);
                events[i].printInfo();
                System.out.println();
            }
        }
}
```

### Класс EventScheduler

```
public class EventScheduler {

    /**
     * Дата мероприятия
     */
    private String date;
    /**
     * Время мероприятия
     */
    private String time;
    /**
     * Длительность мероприятия
     */
    private int duration;
    /**
     * Место проведения
     */
    private String place;
    /**
     * Описание
     */
    private String description;
    /**
     * Участники
     */
    private String[] participants;

    /**
     * Генератор объекта
     * @return созданный объект
     * @throws ParseException при некорректном парсинге даты
     * @throws IOException при некорректном считывании
     */
    public static EventScheduler generate() throws ParseException, IOException {
        Scanner in = new Scanner(System.in);
        EventScheduler event = new EventScheduler();
        System.out.print("Введите дату мероприятия (дд.мм.гггг): ");
        event.setDate(in.nextLine());
        System.out.print("Введите время начала мероприятия (чч:мм): ");
        event.setTime(in.nextLine());
        System.out.print("Введите длительность мероприятия (в минутах): ");
        event.setDuration(in.nextInt());
        in.nextLine();
        System.out.print("Введите место проведения: ");
        event.setPlace(in.nextLine());
        System.out.print("Введите описание мероприятия: ");
        event.setDescription(in.nextLine());
        System.out.print("Введите количество участников: ");
        int amount = in.nextInt();
        in.nextLine();
        event.setParticipants(amount);
        return event;
    }

    /**
     * Получение даты.
     *
     * @return дату
     */
    public String getDate() {
        return this.date;
    }

    /**
     * Установка даты.
     *
     * @param d - дата, которую нужно установить
     * @throws ParseException - если не удалось
     *                        спарсить дату
     */
    public void setDate(final String d) throws ParseException {
            this.date = d;
    }


    /**
     * Получение времени.
     *
     * @return время
     */
    public String getTime() {
        return this.time;
    }

    /**
     * Установка времени.
     *
     * @param t - время, которое нужно установить
     * @throws ParseException - если не удалось
     *                        спарсить время
     */
    public void setTime(String t) throws ParseException {
            this.time = t;
    }

    /**
     * Проверка времени.
     *
     * @param t - время
     * @return - true, если время подходит формату
     */
    private boolean checkTime(String t) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            sdf.setLenient(false);
            sdf.parse(t);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        if (this.checkPlace(place)) {
            this.place = place;
        }
        this.place = place;
    }

    public boolean checkPlace(final String venue) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s[А-Яа-яІі]+)?");
        Matcher matcher = pattern.matcher(venue);
        return matcher.matches();
    }

    public String getDescription() {
        return this.description;
    }

    /**
     * Установка описания мероприятия.
     *
     * @param desc - описание
     */
    public void setDescription(final String desc) {
        if (this.checkDesc(desc)) {
            this.description = desc;
        }
        this.description = desc;
    }

    /**
     * Проверка на корректность ввода описания.
     *
     * @param desc - описание
     * @return true, если строка
     * удовлетворяет регулярному выражению
     */
    private boolean checkDesc(final String desc) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s?[А-Яа-яІі]+)+\\.$");
        Matcher matcher = pattern.matcher(desc);
        return matcher.matches();
    }

    /**
     * Получение участников мероприятия.
     *
     * @return массив участников
     */
    public String[] getParticipants() {
        return this.participants;
    }

    /**
     * Установка участников мероприятия.
     *
     * @param partAmount - количество участников
     * @throws IOException - при некорректном считывании
     */
    public void setParticipants(final int partAmount) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        this.participants = new String[partAmount];
        System.out.format("Введите имена %s участников.%n", partAmount);
        String name;
        for (int i = 0; i < partAmount; i++) {
            System.out.format("Участник #%d: ", i + 1);
            name = reader.readLine();
            if (checkName(name)) {
                this.participants[i] = name;
            } else {
                i--;
            }
        }
    }

    /**
     * Проверка на корректность ввода имени участника.
     *
     * @param name - имя
     * @return true, если строка
     * удовлетворяет регулярному выражению
     */
    private boolean checkName(final String name) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s[А-Яа-яІі]+)?");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /**
     * Вывод информации о мероприятии.
     */
    public void printInfo() {
        System.out.println("Дата: " + this.getDate());
        System.out.println("Время начала: " + this.getTime());
        System.out.println("Длительность: " + this.getDuration() + " минут");
        System.out.println("Место проведения: " + this.getPlace());
        System.out.println("Описание: " + this.getDescription());
        System.out.print("Участники: ");
        for (String name : this.getParticipants()) {
            System.out.print(name + " ");
        }
    }


}

```

### Варіант застосування

Створення списку заходів з детальною інформацією.

![](matushkin-oleksii/doc/demo07/assets/class.png)

_Рисунок 1 - результат створення заходу_

## Висновки

Набув навичок використання об'єктно-орієнтованого підходу для розробки об'єкта предметної галузі.
