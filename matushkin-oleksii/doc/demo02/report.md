# Лабораторна робота №2. Алгоритмічна декомпозиція. Прості алгоритми обробки даних
### Мета: Розробка простих консольних програм для платформи Java SE.
## 1. Вимоги

### 1.1  Розробник

- Матушкін Олексій Дмитрович

- студент групи КІТ-120Б;

- варіант 3

- 10-10-2021.

### 1.2 Загальне завдання

Виконати прикладну задачу згідно з варіантом.

### 1.3 Індивідуальне завдання:

- Знайти найбільшу цифру в десятковому запису цілочисельного значення

## 2. Опис програми

### 2.1 Функціональне призначення

Знаходження найбільшої цифри, що є складновою випадково згенерованного числа.

### 2.2 Логічна структура проекту

**Основний метод**

 ``` public static void main(String[] args) ```
 
 **Призначення:** виклик додаткових функцій


**Метод, що є стартовим для виконання прикладного завдання.**

 ``` private static void highestNumber() ```

 **Опис роботи:** Задає максимальне число доступне для генерації та викликає наступні методи обробки

![](matushkin-oleksii/doc/demo01/assets/highestNumber.png)

_Рисунок 1 - опис функції highestNumber_

**Метод, що генерує псевдо випадкове число**

 ```  private static int setNumber(int maxNumber) ``` 

 **Опис роботи:** метод генерує псевдо-рандомне число за допомогою метода java.util.Random().

 ![](matushkin-oleksii/doc/demo01/assets/setNumber.png)

 _Рисунок 2 - опис функції setNumber_

 **Метод пошуку найбільшої цифри у згенерованному числі**

 ``` private static int findHighestNumber(int lengthOfNumber, int Number) ``` 

 **Опис роботи:** метод виконує пошук найбільшої цифри у згенерованному числі.

 ![](matushkin-oleksii/doc/demo01/assets/findHighestNumber.png)

 _Рисунок 3 - опис функції findHighestNumber_

**Метод виведення даних про поточну ітерацію та результат обчислень на екран**

 ``` private static void outputFormatter(int parameter, int operationCounter) ``` 

 **Опис роботи:** метод виконує форматування і виведення на екран даних про номер поточної ітерації та результату обчислень.

 ![](matushkin-oleksii/doc/demo01/assets/outputFormatter_attempt.png)

 _Рисунок 4 - опис функції outputFormatter_

 **Метод виведення данних про згенероване число на екран**

 ```  private static void outputFormatter(int parameter, int maxNumberLength, int currentNumberLength)) ``` 

 **Опис роботи:** метод виконує форматування і виведення на екран згенерованного числа.

 ![](matushkin-oleksii/doc/demo01/assets/outputFormatter_number.png)
 
 _Рисунок 5 - опис функції outputFormatter_

### 2.2 Важливі фрагменти функції

Код функції highestNumber:

```
private static void highestNumber() {
int maxNumber = 664565; //number should be in range 0 < number < maxNumber
int maxNumberLength = (int) (Math.log10(maxNumber) + 1);
int attempts = 20;
for(int iteration_counter = 0; iteration_counter < attempts; iteration_counter++) {
    int operationCounter = 1;
    outputFormatter(iteration_counter, operationCounter);
    operationCounter++;

    int Number = setNumber(maxNumber); // Generating random number: 0 < number < range
    int lengthOfNumber = (int) (Math.log10(Number) + 1); //Using logarithmic function, then rounding by using (int) and getting count of digits in  number
    outputFormatter(Number, maxNumberLength, lengthOfNumber);
    operationCounter++;

    int highestNumber = findHighestNumber(lengthOfNumber, Number);
    outputFormatter(highestNumber, operationCounter);
    }
}
```

Код функції findHighestNumber:

```
private static int findHighestNumber(int lengthOfNumber, int Number){ //Method finds the highest digit in number
    int highestNumber = 0;
    for (int j = 0; j < lengthOfNumber; j++) {
        int temp = Number % 10;
        if (temp >= highestNumber) highestNumber = temp;
            Number = Number / 10;
    }
return highestNumber;
}
```

Код функції outputFormatter:

```
private static void outputFormatter(int parameter, int operationCounter){
Formatter f = new Formatter(); //java.util class
if (operationCounter == 1) {
    System.out.format("\nAttempt #" + (parameter + 1) + "\t\t\t");
}
else if(operationCounter == 3){
    System.out.format("Highest number is: %d", parameter);
}
}
```

### Варіант застосування

Генерація псевдо-випадкового числа та пошук в ньому найбільшої цифри.

![](matushkin-oleksii/doc/demo01/assets/result.png)

_Рисунок 6 - резултат роботи програми_

## Висновки

В ході даної лабораторної роботи мною було набуто навичок розробки програм для платформи Java SE.
