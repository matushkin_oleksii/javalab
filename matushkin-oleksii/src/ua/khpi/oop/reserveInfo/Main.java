package ua.khpi.oop.reserveInfo;


import java.util.*;

/**
 * Головний класс
 */
public class Main {
    /**
     * Головний метод, що викликає дочірні методи.
     *
     * @param args аргемнти командного рядка
     */
    public static void main(String[] args) {
        List<String> Container = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        String operation;
        int operationInt = 0;
        boolean debugMode = false;
        StringBuilder inputtedText = new StringBuilder();
        while (true) {
            operationInt = 0;
            mainMenuText();
            operation = in.nextLine();
            if (operation.equals("")) {
                operation = "0";
            }
            if (!operation.equals("-h") && !operation.equals("-help") && !operation.equals("-d") && !operation.equals("-debug")) {
                operationInt = Integer.parseInt(operation);
            }
            if (operationInt == 1) {
//                if(Container.size() != 0){
//                    Container.clear();
//                }
                inputtedText = textGetter();
                System.out.println("\nPress \"Enter\" to continue");
                in.nextLine();
            }
            if (operationInt == 2) {
                inputtedText = currentData(inputtedText);
                System.out.println("\nPress \"Enter\" to continue");
                in.nextLine();
            }
            if (operationInt == 3) {
                if (inputtedText.length() != 0) {
                    sentenceFinder(inputtedText, Container);
                } else {
                    System.out.println("You did not enter text");
                    System.out.println("Want to do it now?\n1) Yes\n2) No");
                    int answer = in.nextInt();
                    if (answer == 1) {

                        inputtedText = textGetter();
                        sentenceFinder(inputtedText, Container);
                        System.out.println("Processed successfully\n");
                    } else {
                        operationInt = 0;
                        System.out.println("No data to process");
                    }
                }
                System.out.println("\nPress \"Enter\" to continue");
                in.nextLine();
            } //Process text
            if (operationInt == 4) {
                if (Container.size() != 0)
                    showResult(Container);
                else System.out.println("No data");
            } //Show list
            if (operationInt == 5) {
                Container.clear();
                System.out.println("\n** Bye! **");
                break;
            } //Exit
        }
    }

    /**
     * Метод виконує зчитування тексті, що має підлягти обробці.
     *
     * @return введений користувачем текст
     */
    private static StringBuilder textGetter() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter text: ");
        StringBuilder inputtedText = new StringBuilder(in.nextLine());
        return inputtedText;
    }

    /**
     * Метод приймає повний текст, введений користувачем та піддає обробці, виокремленню речень для подальших обчислень.
     *
     * @param inputtedText строка, містить текст, що підлягає обробці.
     * @param Container    список, у який записується результат виконання обчислень.
     */
    private static void sentenceFinder(StringBuilder inputtedText, List<String> Container) {
        if (Container.size() != 0) {
            Container.clear();
        }
        String currentSentence; //хранение текущего предложения
        int lastIterationIndex = 0; //сюда будет записываться индекс найденного символа (!/?/.)
        char c;
        int currentLength = inputtedText.length();
        for (int i = 0; currentLength > 1; i++) {
            if (i != 0) {
                currentSentence = inputtedText.substring(0, lastIterationIndex); //Запись найденного предложения в отдельную строку для дальнейшей обработки
                inputtedText.delete(0, lastIterationIndex + 2); //Обрезка текста с 0 по знак (!/?/.) т.е. теперь все поле текста будет без найденного
                //ранее предложения. Цикл по его обработке сделаю потом. По идее теперь в inputtedText должен храниться текст без первого предложения.
                //+2 убирает знак и следующий за ним пробел
                Container.add("Current sentence: " + currentSentence);
                //System.out.println("Current sentence: " + currentSentence);
                wordFinder(currentSentence, Container);
            }
            currentLength = inputtedText.length();
            if (currentLength <= 1) {
                break;
            }
            for (int j = 0; j < currentLength; j++) {
                c = inputtedText.charAt(j);
                if (c == '!' || c == '.' || c == '?') {
                    //System.out.println(c); //DEBUG
                    lastIterationIndex = j;
                    i++;
                    currentLength = inputtedText.length();
                    break;
                }
            }
        }
    }

    /**
     * Метод розокремлює речення на окремі слова для подальшого пошуку найбільшого та найменого з них.
     *
     * @param currentSentence строка, містить поточний текст, що підлягає обробці
     * @param Container       список, у який записується результат виконання обчислень.
     */
    static void wordFinder(String currentSentence, List<String> Container) {
        StringBuilder word = new StringBuilder();
        String[] allLargeWords = new String[10]; //Массив наибольших слов
        String[] allSmallWords = new String[10]; //Массив наименьших слов
        String smallWord;
        String largeWord;

        String[] allWords = new String[50]; //Массив слов, размер задан для вмещения 50-ти слов
        int ammounOfWords = 0; //Количество слов в массиве allWords[]

        currentSentence += " "; //В конец пробел, чтобы можно было определить последнее слово

        for (int i = 0; i < currentSentence.length(); i++) { //Перебор предложения в поиске отдельных слов
            char c = currentSentence.charAt(i);
            if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57) || c == '-' || c == '\'')) {
                word.append(currentSentence.charAt(i)); //Запись отдельного слова для дальнейшего помещения в массив allWords[]
            } else if (word.toString().equals("")) {
            } else {
                allWords[ammounOfWords] = word.toString(); //Добавление найденного слова
                ammounOfWords++; //Добавить 1 к счетчику слов
                word = new StringBuilder(); //Очистить StringBuilder для записи нового слова
            }
        }
        smallWord = largeWord = allWords[0]; //Начальное слово

        for (int k = 0; k < ammounOfWords; k++) //Цикл поиска найбольшего и найментшего слов
        {
            if (smallWord.length() > allWords[k].length()) //Если тукущее наименьшее слово больше, чем текущее обрабатываемое слово
            {
                smallWord = allWords[k]; //Заменяем наименьшее
            }
            if (largeWord.length() < allWords[k].length()) //Если тукущее наибольшее слово больше, чем текущее обрабатываемое слово
            {
                largeWord = allWords[k]; //Заменяем наибольшее
            }
        }
        int smallCounter = 0;
        int largeCounter = 0;
        for (int k = 0; k < ammounOfWords; k++) //Цикл поиска равных по длинне слов
        {
            if (largeWord.length() == allWords[k].length()) {
                allLargeWords[largeCounter] = allWords[k];
                largeCounter++;
            }

            if (smallWord.length() == allWords[k].length()) {
                allSmallWords[smallCounter] = allWords[k];
                smallCounter++;
            }
        }
        Container.add("\nResult of processing: ");
        OutputFormatter(1, allSmallWords, smallCounter, Container);
        OutputFormatter(1, allLargeWords, largeCounter, Container);
        Container.add("\n");
    } //Works correctly

    /**
     * Метод відповідає за форматування тексту, що має виводитись у командний рядок.
     *
     * @param parameter вказує на дію, що підлягає виконанню.
     * @param words     містить слово, що підлягає виведенню у командний рядок.
     * @param count     кількість об'єктів у контейнері.
     * @param Container список, у який записується результат виконання обчислень.
     */
    private static void OutputFormatter(int parameter, String[] words, int count, List<String> Container) {        //Method formats output information
        Formatter f = new Formatter(); //java.util class
        if (parameter == 1) {
            f.format("The smallest words are: ");
            for (int i = 0; i < count; i++) {
                if (i == 0)
                    f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        } else if (parameter == 2) {
            f.format("The largest words are: ");
            for (int i = 0; i < count; i++) {
                if (i == 0)
                    f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        }
        String string = f.toString();
        Container.add(string);
    }

    /**
     * Метод виводить текст головного меню.
     */
    private static void mainMenuText() {
        System.out.println("\t\tTextProcessor menu");
        System.out.println("Enter the operation you want to do: ");
        System.out.println("1) Enter data");
        System.out.println("2) Current data");
        System.out.println("3) Operate with data");
        System.out.println("4) Show result");
        System.out.println("5) Exit");
    }

    /**
     * Метод виводить поточні дані у командний рядок, такі як: текст, що підлягає обробці або заміні, та чи увімкнено режим відлагодження.
     *
     * @param inputtedText строка, що містить поточний введений текст, що має підлягти обробці.
     * @return поточні данні
     */
    private static StringBuilder currentData(StringBuilder inputtedText) {
        Scanner in = new Scanner(System.in);
        if (inputtedText.length() == 0) {
            System.out.println("You did not enter text");
            System.out.println("Want to do it now?\n1) Yes\n2) No");
            int answer = in.nextInt();
            if (answer == 1) {
                inputtedText = textGetter();
                System.out.println("Inputted text: " + inputtedText);
                return inputtedText;
            } else System.out.println("No data");
        } else {
            System.out.println("Current text: " + inputtedText);
        }
        return inputtedText;
    }

    /**
     * Виводить результат виконання обчислень, зроблених у процесі роботи, у командний рядок.
     *
     * @param Container список, що містить результуючі дані, які мають виводитись на екран після надання певної команди.
     */
    public static void showResult(List<String> Container) {
        for (int i = 0; i < Container.size(); i++) {
            System.out.println(Container.get(i));
        }
        System.out.println();
    }

    //Check text
    //MARTINEZ: And it's not just food. Oil prices keep climbing, prices for goods like cars. Used cars are going up again. Supply chains are still a mess, which means that inflation numbers will probably still be elevated when the Labor Department offers its monthly consumer prices snapshot this morning.
    //Looking started he up perhaps against. How remainder all additions get elsewhere resources. One missed shy wishes supply design answer formed. Prevent on present hastily passage an subject in be. Be happiness arranging so newspaper defective affection ye. Families blessing he in to no daughter. Far concluded not his something extremity. Want four we face an he gate. On he of played he ladies answer little though nature. Blessing oh do pleasure as so formerly. Took four spot soon led size you. Outlived it received he material. Him yourself joy moderate off repeated laughter outweigh screened. Wrote water woman of heart it total other. By in entirely securing suitable graceful at families improved. Zealously few furniture repulsive was agreeable consisted difficult. Collected breakfast estimable questions in to favourite it. Known he place worth words it as to. Spoke now noise off smart her ready. He do subjects prepared bachelor juvenile ye oh. He feelings removing informed he as ignorant we prepared. Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold ye. Be call four my went mean. Celebrated if remarkably especially an. Going eat set she books found met aware. By impossible of in difficulty discovered celebrated ye. Justice joy manners boy met resolve produce. Bed head loud next plan rent had easy add him. As earnestly shameless elsewhere defective estimable fulfilled of. Esteem my advice it an excuse enable. Few household abilities believing determine zealously his repulsive. To open draw dear be by side like.Sitting mistake towards his few country ask. You delighted two rapturous six depending objection happiness something the. Off nay impossible dispatched partiality unaffected. Norland adapted put ham cordial. Ladies talked may shy basket narrow see. Him she distrusts questions sportsmen. Tolerably pretended neglected on my earnestly by. Sex scale sir style truth ought.

}

