package ua.khpi.oop.reserveInfo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventScheduler {

    /**
     * Дата мероприятия
     */
    private Date date;
    /**
     * Время мероприятия
     */
    private Date time;
    /**
     * Длительность мероприятия
     */
    private int duration;
    /**
     * Место проведения
     */
    private String place;
    /**
     * Описание
     */
    private String description;
    /**
     * Участники
     */
    private String[] participants;

    /**
     * Генератор объекта
     * @return созданный объект
     * @throws ParseException при некорректном парсинге даты
     * @throws IOException при некорректном считывании
     */
    public static EventScheduler generate() throws ParseException, IOException {
        Scanner in = new Scanner(System.in);
        EventScheduler event = new EventScheduler();
        System.out.print("Введите дату мероприятия (дд.мм.гггг): ");
        event.setDate(in.nextLine());
        System.out.print("Введите время начала мероприятия (чч:мм): ");
        event.setTime(in.nextLine());
        System.out.print("Введите длительность мероприятия (в минутах): ");
        event.setDuration(in.nextInt());
        in.nextLine();
        System.out.print("Введите место проведения: ");
        event.setPlace(in.nextLine());
        System.out.print("Введите описание мероприятия: ");
        event.setDescription(in.nextLine());
        System.out.print("Введите количество участников: ");
        int amount = in.nextInt();
        in.nextLine();
        event.setParticipants(amount);
        return event;
    }

    /**
     * Получение даты.
     *
     * @return дату
     */
    public String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(this.date);
    }

    /**
     * Установка даты.
     *
     * @param d - дата, которую нужно установить
     * @throws ParseException - если не удалось
     *                        спарсить дату
     */
    public void setDate(final String d) throws ParseException {
        if (this.checkDate(d)) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            this.date = sdf.parse(d);
        }
    }

    /**
     * Проверка даты.
     *
     * @param d - дата
     * @return - true, если дата подходит формату
     */
    private boolean checkDate(final String d) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            sdf.setLenient(false);
            sdf.parse(d);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * Получение времени.
     *
     * @return время
     */
    public String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(this.time);
    }

    /**
     * Установка времени.
     *
     * @param t - время, которое нужно установить
     * @throws ParseException - если не удалось
     *                        спарсить время
     */
    public void setTime(String t) throws ParseException {
        if (checkTime(t)) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            this.time = sdf.parse(t);
        }
    }

    /**
     * Проверка времени.
     *
     * @param t - время
     * @return - true, если время подходит формату
     */
    private boolean checkTime(String t) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            sdf.setLenient(false);
            sdf.parse(t);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        if (this.checkPlace(place)) {
            this.place = place;
        }
        this.place = place;
    }

    public boolean checkPlace(final String venue) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s[А-Яа-яІі]+)?");
        Matcher matcher = pattern.matcher(venue);
        return matcher.matches();
    }

    public String getDescription() {
        return this.description;
    }

    /**
     * Установка описания мероприятия.
     *
     * @param desc - описание
     */
    public void setDescription(final String desc) {
        if (this.checkDesc(desc)) {
            this.description = desc;
        }
        this.description = desc;
    }

    /**
     * Проверка на корректность ввода описания.
     *
     * @param desc - описание
     * @return true, если строка
     * удовлетворяет регулярному выражению
     */
    private boolean checkDesc(final String desc) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s?[А-Яа-яІі]+)+\\.$");
        Matcher matcher = pattern.matcher(desc);
        return matcher.matches();
    }

    /**
     * Получение участников мероприятия.
     *
     * @return массив участников
     */
    public String[] getParticipants() {
        return this.participants;
    }

    /**
     * Установка участников мероприятия.
     *
     * @param partAmount - количество участников
     * @throws IOException - при некорректном считывании
     */
    public void setParticipants(final int partAmount) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        this.participants = new String[partAmount];
        System.out.format("Введите имена %s участников.%n", partAmount);
        String name;
        for (int i = 0; i < partAmount; i++) {
            System.out.format("Участник #%d: ", i + 1);
            name = reader.readLine();
            if (checkName(name)) {
                this.participants[i] = name;
            } else {
                i--;
            }
        }
    }

    /**
     * Проверка на корректность ввода имени участника.
     *
     * @param name - имя
     * @return true, если строка
     * удовлетворяет регулярному выражению
     */
    private boolean checkName(final String name) {
        Pattern pattern = Pattern.compile("^[А-Яа-яІі]+(\\s[А-Яа-яІі]+)?");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /**
     * Вывод информации о мероприятии.
     */
    public void printInfo() {
        System.out.println("Дата: " + this.getDate());
        System.out.println("Время начала: " + this.getTime());
        System.out.println("Длительность: " + this.getDuration() + " минут");
        System.out.println("Место проведения: " + this.getPlace());
        System.out.println("Описание: " + this.getDescription());
        System.out.print("Участники: ");
        for (String name : this.getParticipants()) {
            System.out.print(name + " ");
        }
    }


}
