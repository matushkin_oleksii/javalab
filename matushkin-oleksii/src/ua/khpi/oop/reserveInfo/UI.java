package ua.khpi.oop.reserveInfo;


import ua.khpi.oop.matushkin07.EventScheduler;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.text.ParseException;
import java.util.Scanner;

public final class UI {
    /**
     * Для чтения данных с клавиатуры.
     */
    private static BufferedReader buffer = new BufferedReader(
            new InputStreamReader(System.in));

    private UI() {
    }

    /**
     * Главное меню.
     */
    public static void mainMenu() {
        System.out.println("1. Добавление мероприятия.");
        System.out.println("2. Удаление мероприятия.");
        System.out.println("3. Вывод информации.");
        System.out.println("4. Сохранить в файл.");
        System.out.println("5. Загрузить из файла.");
        System.out.println("0. Выход.");
        System.out.print("Введите ваш ответ сюда: ");
    }

    /**
     * Получение ответа диалогового меню.
     *
     * @return ответ
     * @throws IOException при неудачном считывании
     */
    public static String getChoice() throws IOException {
        return buffer.readLine();
    }

    /**
     * Добавление мероприятия.
     *
     * @param events - исходный набор мероприятий
     * @return новый набор мероприятий
     * @throws IOException    при неудачном считывании
     * @throws ParseException при неудачном парсинге
     */
    public static EventScheduler[] addEvent(final EventScheduler[] events)
            throws IOException, ParseException {
        EventScheduler[] newEvents = new EventScheduler[events.length + 1];
        System.arraycopy(events, 0, newEvents, 0, events.length);
        newEvents[events.length] = EventScheduler.generate(true);
        return newEvents;
    }

    /**
     * Удаление мероприятия.
     *
     * @param events - исходный набор мероприятий
     * @param pos    - позиция удаляемого мероприятия
     * @return новый набор мероприятий
     */
    public static EventScheduler[] dropEvent(final EventScheduler[] events,
                                             final int pos) {
        if (pos >= events.length) {
            System.out.println("Error. Out of bounds.");
            return events;
        } else {
            EventScheduler[] newEvents = new EventScheduler[events.length - 1];
            int i = 0;
            for (int j = 0; j < events.length; j++) {
                if (j == pos) {
                    continue;
                } else {
                    newEvents[i] = events[j];
                    i++;
                }
            }
            return newEvents;
        }
    }

    /**
     * Вывод информации о мероприятиях.
     *
     * @param events - набор мероприятий
     */
    public static void printInfo(EventScheduler[] events) {
        for (int i = 0; i < events.length; i++) {
            System.out.format("%nМЕРОПРИЯТИЕ %d:%n", i + 1);
            events[i].printInfo();
        }
    }

//    /**
//     * Сохранение объекта в файл.
//     *
//     * @param events - набор мероприятий
//     * @throws IOException при неудачном
//     *                     считывании или записи.
//     */
//    public static void saveToFile(final EventScheduler[] events)
//            throws IOException {
//        final String semi = ";";
//        Pattern pattern = Pattern.compile(semi);
//        Matcher matcher;
//        String path;
//        StringBuilder direct = new StringBuilder();
//        System.out.print("Введите имя директории: ");
//        while (true) {
//            System.out.print(direct.toString());
//            path = buffer.readLine();
//            direct.append(path).append("\\");
//            matcher = pattern.matcher(direct.toString());
//            if (matcher.find()) {
//                break;
//            }
//            File directory = new File(direct.toString());
//            File[] list = directory.listFiles();
//            if (list == null) {
//                System.out.println("Неверное"
//                        + " имя директории!");
//                if (direct.length() != 0) {
//                    direct.delete(
//                            direct.length() - path.length() - 1,
//                            direct.length());
//                }
//                continue;
//            }
//            System.out.println("---------------");
//            for (File it : list) {
//                if (it.isDirectory()) {
//                    System.out.print(it.getName());
//                    System.out.println(" (...)");
//                    continue;
//                }
//                System.out.println(it.getName());
//            }
//            System.out.println("---------------");
//        }
//        String currentDir = direct.toString();
//        currentDir = currentDir.replaceAll(semi, "");
//        FileOutputStream fos = new FileOutputStream(
//                currentDir + "\\Serialized.txt");
//        XMLEncoder xmlEncoder = new XMLEncoder(new BufferedOutputStream(fos));
//        xmlEncoder.writeObject(events);
//        xmlEncoder.close();
//    }

    public static void FolderMenu(Scanner inInt, Scanner inStr, EventScheduler[] list) {
        boolean endProcess = false;
        boolean folderChoose = true;
        int menu;
        String absolutePath = new File("").getAbsolutePath();
        File folder = new File(absolutePath);
        File[] listFiles = folder.listFiles();
        String currentDir = absolutePath;
        String highestDir = folder.getName();
        int position = 0;

        while(!endProcess) {
            System.out.println("\nCurrent path: " + currentDir);
            System.out.println("\nFiles and directories in this path: ");
            for (position = 0; position < listFiles.length; position++)
                System.out.println(position + 1 + ". " + listFiles[position].toString().substring(currentDir.length() + 1));
            System.out.println();
            System.out.println("Process menu: ");
            System.out.println("1. Work in current directory");
            System.out.println("2. Go up one level folder");
            System.out.println("3. Enter the folder");
            System.out.println("4. End of process");
            System.out.print("Enter option: ");
            menu = inInt.nextInt();
            System.out.println();
            switch(menu) {
                case 1:
                    StartWork(inInt, inStr, currentDir, position, listFiles, list);
                    endProcess = true;
                    break;
                case 2:
                    if(folder.getName().equals(highestDir)) {
                        System.out.print("This is the highest directory.");
                        break;
                    }
                    currentDir = currentDir.substring(0, currentDir.indexOf(folder.getName())-1);
                    folder = new File(currentDir);
                    listFiles = folder.listFiles();
                    break;
                case 3:
                    while(folderChoose) {
                        System.out.print("Choose the number of folder: ");
                        position = inInt.nextInt();
                        if(!listFiles[position - 1].isDirectory() || position < 1 || position > listFiles.length)
                            System.out.println("Error. That is not a folder.");
                        else {
                            currentDir = listFiles[position - 1].toString();
                            System.out.println("New current directory: " + currentDir);
                            folder = new File(currentDir);
                            listFiles = folder.listFiles();
                            folderChoose = false;
                        }
                    }
                    break;
                case 4:
                    System.out.println("End of process");
                    System.out.println();
                    endProcess = true;
                    break;
                default:
                    System.out.println("Error! Wrong num in menu.");
                    break;
            }
        }
    }

    public static void StartWork(Scanner inInt, Scanner inStr, String currentDir, int position, File[] listFiles, EventScheduler[] list) {
        String filename;
        System.out.println("Choose: ");
        System.out.println("1. Serialization");
        System.out.println("2. Deserialization");
        System.out.print("Enter option: ");
        int menu = inInt.nextInt();
        switch(menu) {
            case 1:
                System.out.print("Enter the name of XML file: ");
                filename = inStr.nextLine();
                if (filename.indexOf(".xml") == -1)
                    filename += ".xml";
                System.out.println("XML file name: " + filename);
                String absolutePath = currentDir;
                File folder = new File(absolutePath);
                File file = new File(folder,filename);
                try {
                    XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file)));
                    encoder.writeObject(list);
                    encoder.close();
                }
                catch (Exception e) {
                    System.out.println(e);
                    break;
                }
                System.out.println("File was written in this directory: " + absolutePath);
                System.out.println("Serialization was completed.");
                break;
            case 2:
                System.out.print("Enter number of the file: ");
                position = inInt.nextInt();
                absolutePath = currentDir + "\\" + listFiles[position - 1].getName();
                file = new File(absolutePath);

                if(listFiles[position - 1].getName().indexOf(".xml") == -1 || listFiles[position - 1].isDirectory()) {
                    System.out.println("Error, that's not a .XML file.");
                    break;
                }
                try {
                    XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(file)));
                    list = (EventScheduler[])decoder.readObject();
                    decoder.close();
                }
                catch (Exception e) {
                    System.out.println(e);
                    break;
                }
                System.out.println("File was read from this directory: " + listFiles[position - 1]);
                System.out.println("Deserialization was completed.");
                break;
        }

    }



    /**
     * Загрузка объекта из файла.
     *
     * @return массив объектов
     * @throws IOException при неудачном
     *                     считывании или записи.
     */
    public static EventScheduler[] loadFromFile() throws IOException {
        System.out.print("Введите имя директории: ");
        String dirToExtract = buffer.readLine();
        FileInputStream fis = new FileInputStream(dirToExtract);
        XMLDecoder xmlDecoder = new XMLDecoder(new BufferedInputStream(fis));
        EventScheduler[] getEvents = (EventScheduler[]) xmlDecoder.readObject();
        xmlDecoder.close();
        return getEvents;
    }
}

