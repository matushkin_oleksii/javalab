package ua.khpi.oop.matushkin04;

import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

/**
 * �������� �����
 */
public class TextProcessor {

    public static void main(String[] args) {
        System.out.println("");
    }

    /**
     * ����� ���������� ��� ���������� ����� ������������, ������ ���� ������� ����������.
     * ����� ������ ������ �����, �������� ������������ �� ���� �������, ������������ ������ ��� ��������� ���������.
     *  @param inputtedText ������, ������ �����, �� ������ �������.
     *
     */
    public static void sentenceFinder(StringBuilder inputtedText) {
//        if (!Container.isEmpty()) {
//            Container.clear();
//        }
//        if (debugMode) {
//            sentenceFinderDebug(inputtedText, Container);
//        }
        //�������� �������� �����������
        String currentSentence;
        int lastIterationIndex = 0; //���� ����� ������������ ������ ���������� ������� (!/?/.)
        char c;
        int currentLength = inputtedText.length();
        for (int i = 0; currentLength > 1; i++) {
            if (i != 0) {
                currentSentence = inputtedText.substring(0, lastIterationIndex); //������ ���������� ����������� � ��������� ������ ��� ���������� ���������
                inputtedText.delete(0, lastIterationIndex + 2); //������� ������ � 0 �� ���� (!/?/.) �.�. ������ ��� ���� ������ ����� ��� ����������
                //����� �����������. ���� �� ��� ��������� ������ �����. �� ���� ������ � inputtedText ������ ��������� ����� ��� ������� �����������.
                //+2 ������� ���� � ��������� �� ��� ������
//                Container.add("Current sentence: " + currentSentence);
//                //System.out.println("Current sentence: " + currentSentence);
//                wordFinder(currentSentence, Container);
            }
            currentLength = inputtedText.length();
            if (currentLength <= 1) {
                break;
            }
            for (int j = 0; j < currentLength; j++) {
                c = inputtedText.charAt(j);
                if (c == '!' || c == '.' || c == '?') {
                    //System.out.println(c); //DEBUG
                    lastIterationIndex = j;
                    i++;
                    currentLength = inputtedText.length();
                    break;
                }
            }
        }
    }

    /**
     * ����� ���������� ��� ���������� ����� ������������, ������ ����� ����������, �� ���� ��������� � ������ �������� �������������.
     * ����� ������ ������ �����, �������� ������������ �� ���� �������, ������������ ������ ��� ��������� ���������.
     *
     * @param inputtedText ������, ������ �����, �� ������ �������.
     * @param Container    ������, � ���� ���������� ��������� ��������� ���������.
     */
    public static void sentenceFinderDebug(StringBuilder inputtedText, List<String> Container) {
        //System.out.println("\nDEBUG MODE ENTERED");
        System.out.println("\nDEBUG MODE ENTERED");
        //System.out.println("������� ������: ");
        System.out.println("\n������� ������: ");
        //System.out.println("�������� �����: " + inputtedText);
        System.out.println("\n�������� �����: " + inputtedText);
        String currentSentence; //�������� �������� �����������
        int lastIterationIndex = 0; //���� ����� ������������ ������ ���������� ������� (!/?/.)
        char c;
        int currentLength = inputtedText.length();
        for (int i = 0; currentLength > 1; i++) {
            if (i != 0) {
                currentSentence = inputtedText.substring(0, lastIterationIndex);
                System.out.println("������� ������ ��������������� ������: " + currentLength);
                //System.out.println("��������� ��������� ����: " + "\"" + inputtedText.charAt(lastIterationIndex) + "\"" + ". ��� ������: " + lastIterationIndex);
                System.out.println("��������� ��������� ����: " + "\"" + inputtedText.charAt(lastIterationIndex) + "\"" + ". ��� ������: " + lastIterationIndex);
                //System.out.println("�������� ��������� �����������: " + currentSentence);
                System.out.println("\n�������� ��������� �����������: " + currentSentence);
                //������ ���������� ����������� � ��������� ������ ��� ���������� ���������
                //System.out.println("����������� ��������� � �������� ������... ��������� ����������� ��������.");
                System.out.println("\n����������� ��������� � �������� ������... ��������� ����������� ��������.");
                inputtedText.delete(0, lastIterationIndex + 2); //������� ������ � 0 �� ���� (!/?/.) �.�. ������ ��� ���� ������ ����� ��� ����������
                //����� �����������. ���� �� ��� ��������� ������ �����. �� ���� ������ � inputtedText ������ ��������� ����� ��� ������� �����������.
                //+2 ������� ���� � ��������� �� ��� ������
                //System.out.println("���������� ������: ");
                System.out.println("\n���������� ������: ");
                if (inputtedText.length() != 0) {
                    //System.out.println("������� �������� �����: " + inputtedText + "\n������� ������ ������: " + inputtedText.length());
                    System.out.println("������� �������� �����: " + inputtedText + "\n������� ������ ������: " + inputtedText.length());
                } else {
                    //System.out.println("������� �������� �����: �����");
                    System.out.println("������� �������� �����: �����");
                }
                //System.out.println("�������� � ����� ���������� ��������� �����������: " + currentSentence);
                System.out.println("\n�������� � ����� ���������� ��������� �����������: " + currentSentence);
                wordFinderDebug(currentSentence, Container);
            }
            currentLength = inputtedText.length();
            if (currentLength <= 1) {
                break;
            }
            for (int j = 0; j < currentLength; j++) {
                c = inputtedText.charAt(j);
                if (c == '!' || c == '.' || c == '?') {
                    lastIterationIndex = j;
                    i++;
                    currentLength = inputtedText.length();
                    break;
                }
            }
        }
        //System.out.println("#���������� ������ ������#");
        System.out.println("\n#���������� ������ ������#");
    }

    /**
     * ����� ���������� ��� ���������� ����� ������������, ������ ���� ������� ����������.
     * ����� ����������� ������� �� ����� ����� ��� ���������� ������ ���������� �� ��������� � ���.
     *
     * @param currentSentence ������, ������ �������� �����, �� ������ �������
     * @param Container       ������, � ���� ���������� ��������� ��������� ���������.
     */
    public static void wordFinder(String currentSentence, List<String> Container) {
        StringBuilder word = new StringBuilder();
        String[] allLargeWords = new String[10]; //������ ���������� ����
        String[] allSmallWords = new String[10]; //������ ���������� ����
        String smallWord;
        String largeWord;

        String[] allWords = new String[50]; //������ ����, ������ ����� ��� �������� 50-�� ����
        int ammounOfWords = 0; //���������� ���� � ������� allWords[]

        currentSentence += " "; //� ����� ������, ����� ����� ���� ���������� ��������� �����

        for (int i = 0; i < currentSentence.length(); i++) { //������� ����������� � ������ ��������� ����
            char c = currentSentence.charAt(i);
            if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57) || c == '-' || c == '\'')) {
                word.append(currentSentence.charAt(i)); //������ ���������� ����� ��� ����������� ��������� � ������ allWords[]
            } else if (word.toString().equals("")) {
            } else {
                allWords[ammounOfWords] = word.toString(); //���������� ���������� �����
                ammounOfWords++; //�������� 1 � �������� ����
                word = new StringBuilder(); //�������� StringBuilder ��� ������ ������ �����
            }
        }
        smallWord = largeWord = allWords[0]; //��������� �����

        for (int k = 0; k < ammounOfWords; k++) //���� ������ ����������� � ����������� ����
        {
            if (smallWord.length() > allWords[k].length()) //���� ������� ���������� ����� ������, ��� ������� �������������� �����
            {
                smallWord = allWords[k]; //�������� ����������
            }
            if (largeWord.length() < allWords[k].length()) //���� ������� ���������� ����� ������, ��� ������� �������������� �����
            {
                largeWord = allWords[k]; //�������� ����������
            }
        }
        int smallCounter = 0;
        int largeCounter = 0;
        for (int k = 0; k < ammounOfWords; k++) //���� ������ ������ �� ������ ����
        {
            if (largeWord.length() == allWords[k].length()) {
                allLargeWords[largeCounter] = allWords[k];
                largeCounter++;
            }

            if (smallWord.length() == allWords[k].length()) {
                allSmallWords[smallCounter] = allWords[k];
                smallCounter++;
            }
        }
        Container.add("\n��������� ���������: ");
        outputFormatter(1, allSmallWords, smallCounter, Container);
        outputFormatter(1, allLargeWords, largeCounter, Container);
        Container.add("\n");
    } //Works correctly

    /**
     * ����� ���������� ��� ���������� ����� ������������, ������ ����� ����������, �� ���� ��������� � ������ �������� �������������.
     * ����� ����������� ������� �� ����� ����� ��� ���������� ������ ���������� �� ��������� � ���.
     *
     * @param currentSentence ������, ������ �������� �����, �� ������ �������
     * @param Container       ������, � ���� ���������� ��������� ��������� ���������.
     */
    public static void wordFinderDebug(String currentSentence, List<String> Container) {
        System.out.println("\n**���������� ������ wordFinder**");
        StringBuilder word = new StringBuilder();
        String[] allLargeWords = new String[10]; //������ ���������� ����
        String[] allSmallWords = new String[10]; //������ ���������� ����
        String smallWord;
        String largeWord;

        String[] allWords = new String[50]; //������ ����, ������ ����� ��� �������� 50-�� ����
        int ammounOfWords = 0; //���������� ���� � ������� allWords[]

        currentSentence += " "; //� ����� ������, ����� ����� ���� ���������� ��������� �����
        System.out.println("\n��������� �����������: " + currentSentence);

        for (int i = 0; i < currentSentence.length(); i++) { //������� ����������� � ������ ��������� ����
            char c = currentSentence.charAt(i);
            if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57) || c == '-' || c == '\'')) {
                word.append(currentSentence.charAt(i)); //������ ���������� ����� ��� ����������� ��������� � ������ allWords[]
            } else if (word.toString().equals("")) {
            } else {
                allWords[ammounOfWords] = word.toString(); //���������� ���������� �����
                ammounOfWords++; //�������� 1 � �������� ����
                word = new StringBuilder(); //�������� StringBuilder ��� ������ ������ �����
            }
        }
        smallWord = largeWord = allWords[0]; //��������� �����

        for (int k = 0; k < ammounOfWords; k++) //���� ������ ����������� � ����������� ����
        {
            if (smallWord.length() > allWords[k].length()) //���� ������� ���������� ����� ������, ��� ������� �������������� �����
            {
                smallWord = allWords[k]; //�������� ����������
            }
            if (largeWord.length() < allWords[k].length()) //���� ������� ���������� ����� ������, ��� ������� �������������� �����
            {
                largeWord = allWords[k]; //�������� ����������
            }
        }
        int smallCounter = 0;
        int largeCounter = 0;
        for (int k = 0; k < ammounOfWords; k++) //���� ������ ������ �� ������ ����
        {
            if (largeWord.length() == allWords[k].length()) {
                allLargeWords[largeCounter] = allWords[k];
                largeCounter++;
            }

            if (smallWord.length() == allWords[k].length()) {
                allSmallWords[smallCounter] = allWords[k];
                smallCounter++;
            }
        }
        System.out.println("\n��������� ���������: ");
        outputFormatter(1, allSmallWords, smallCounter, Container);
        outputFormatter(1, allLargeWords, largeCounter, Container);
        Container.add("\n");
    } //Works correctly

    /**
     * ����� ������� �� ������������ ������, �� �� ���������� � ��������� �����.
     *
     * @param parameter ����� �� ��, �� ������ ���������.
     * @param words     ������ �����, �� ������ ��������� � ��������� �����.
     * @param count     ������� ��'���� � ���������.
     * @param Container ������, � ���� ���������� ��������� ��������� ���������.
     */
    public static void outputFormatter(int parameter, String[] words, int count, List<String> Container) {        //Method formats output information
        Formatter f = new Formatter(); //java.util class
        if (parameter == 1) {
            f.format("The smallest words are: ");
            for (int i = 0; i < count; i++) {
                if (i == 0)
                    f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        } else if (parameter == 2) {
            f.format("The largest words are: ");
            for (int i = 0; i < count; i++) {
                if (i == 0)
                    f.format(words[i]);
                else
                    f.format(", " + words[i]);
            }
            f.format("\t\t");
        }
        String string = f.toString();
        Container.add(string);
    }

    /**
     * ����� �������� ������� ���� � ��������� �����, ��� ��: �����, �� ������ ������� ��� �����, �� �� �������� ����� ������������.
     *
     * @param inputtedText ������, �� ������ �������� �������� �����, �� �� ������� �������.
     * @param debugMode    ������� ��������, �� ������� ��� �������� �� ��������� ������ ������������.
     * @return ������� �����
     */
    public static StringBuilder currentData(StringBuilder inputtedText, boolean debugMode) {
        Scanner in = new Scanner(System.in);
        if (inputtedText.length() == 0) {
            System.out.println("You did not enter text");
            System.out.println("Want to do it now?\n1) Yes\n2) No");
            int answer = in.nextInt();
            if (answer == 1) {
                System.out.println("Enter text:");
                in.nextLine();
                inputtedText = new StringBuilder(in.nextLine());
                System.out.println("Inputted text: " + inputtedText);
                return inputtedText;
            }
        } else {
            System.out.println("Current text: " + inputtedText);
        }
        System.out.println("Debug mode: " + debugMode);
        return inputtedText;
    }

    /**
     * �������� ��������� ��������� ���������, ��������� � ������ ������, � ��������� �����.
     *
     * @param Container ������, �� ������ ����������� ����, �� ����� ���������� �� ����� ���� ������� ����� �������.
     */
    public static void showResult(List<String> Container) {
        for (String s : Container) {
            System.out.println(s);
        }
        System.out.println();
    }

    /**
     * ����� ��������� ��������� �� ��������� ������ ������������.
     *
     * @param debugMode ������ ���������� �� �������� ����� ������������.
     * @return ������ �� ��������� �� ��������� ������ ������������.
     */
    public static boolean debugModeControl(boolean debugMode) {
        if (!debugMode) {
            System.out.println("Debug mode: ON");
            debugMode = true;
        } else {
            System.out.println("Debug mode: OFF");
            debugMode = false;
        }
        return debugMode;
    }

    /**
     * ����� ������� �� ��������� ���������� ��� �������� �����.
     */
    public static void flagHelp() {
        System.out.println("\n�����������: �������� �������");
        System.out.println("������� ������ ���-120�");
        System.out.println("�������������� ���������: ������ �����. ����� ������� �� �������.\n" +
                "��� ������� ������� ������ �� ����������� �� ����� �����������\n" +
                "�� �� ����� ��������� �������.\n");
        System.out.println("*�������� ������� ����*");
        System.out.println("Enter data-��������� ���� ������ ��� ���������");
        System.out.println("Current data-����� ������� ���������� (������, ���������������� ��� ���������)");
        System.out.println("Operate with data-������ ��������� ����������");
        System.out.println("Show result-������� ���������");
        System.out.println("Exit-����� �� ���������");
        System.out.println("*���������� � ������*");
        System.out.println("-h ��� -help :" +
                " ������������ ���������� ��� ������ ��������,\n" +
                "����������� (������������ ��������), ��������� ���� ������\n" +
                "������ (������ ���� �� ��������� ����������\n�����)");
        System.out.println("-d ��� -debug : � ������ ������ �������� ������������� ��������\n" +
                "����, �� ���������� ������������ �� �������� ������������� ��������:\n" +
                "����������� �����������, ������� �������� ������, ��������\n" +
                "���������� ������ �� ��.");

    }

    //Check text
    //MARTINEZ: And it's not just food. Oil prices keep climbing, prices for goods like cars. Used cars are going up again. Supply chains are still a mess, which means that inflation numbers will probably still be elevated when the Labor Department offers its monthly consumer prices snapshot this morning.
    //Looking started he up perhaps against. How remainder all additions get elsewhere resources. One missed shy wishes supply design answer formed. Prevent on present hastily passage an subject in be. Be happiness arranging so newspaper defective affection ye. Families blessing he in to no daughter. Far concluded not his something extremity. Want four we face an he gate. On he of played he ladies answer little though nature. Blessing oh do pleasure as so formerly. Took four spot soon led size you. Outlived it received he material. Him yourself joy moderate off repeated laughter outweigh screened. Wrote water woman of heart it total other. By in entirely securing suitable graceful at families improved. Zealously few furniture repulsive was agreeable consisted difficult. Collected breakfast estimable questions in to favourite it. Known he place worth words it as to. Spoke now noise off smart her ready. He do subjects prepared bachelor juvenile ye oh. He feelings removing informed he as ignorant we prepared. Evening do forming observe spirits is in. Country hearted be of justice sending. On so they as with room cold ye. Be call four my went mean. Celebrated if remarkably especially an. Going eat set she books found met aware. By impossible of in difficulty discovered celebrated ye. Justice joy manners boy met resolve produce. Bed head loud next plan rent had easy add him. As earnestly shameless elsewhere defective estimable fulfilled of. Esteem my advice it an excuse enable. Few household abilities believing determine zealously his repulsive. To open draw dear be by side like.Sitting mistake towards his few country ask. You delighted two rapturous six depending objection happiness something the. Off nay impossible dispatched partiality unaffected. Norland adapted put ham cordial. Ladies talked may shy basket narrow see. Him she distrusts questions sportsmen. Tolerably pretended neglected on my earnestly by. Sex scale sir style truth ought.
/*
C:/Users/lesha/.jdks/openjdk-17.0.1/bin/java.exe S:/University/JavaTasksKhPI/javalab/matushkin-oleksii/src/ua/khpi/oop/matushkin04/TextProcessor.java -h
java TextProcessor.java "-h"
*/

}

