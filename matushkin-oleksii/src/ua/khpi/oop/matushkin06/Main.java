package ua.khpi.oop.matushkin06;

import ua.khpi.oop.lukianchykova03.SameFirstLast;
import ua.khpi.oop.matushkin04.TextProcessor;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        AContainerMod container = new AContainerMod();
        boolean continueWorking = true;
        Scanner stringIn = new Scanner(System.in);
        Scanner integerIn = new Scanner(System.in);
        while (continueWorking) {
            System.out.println("Main menu:");
            System.out.println("1. New data");
            System.out.println("2. Show data");
            System.out.println("3. Add element");
            System.out.println("4. Delete element");
            System.out.println("5. Clear container");
            System.out.println("6. Sort container");
            System.out.println("7. Find element");
            System.out.println("8. Compare elements");
            System.out.println("9. Someones class");
            System.out.println("10. My class");
            System.out.println("11. Serialize container");
            System.out.println("12. Deserialize");
            System.out.println("13. Exit");
            System.out.println("Enter option:");
            int option = integerIn.nextInt();
            System.out.println();
            switch (option) {
                case 1 -> {
                    container.clear();
                    System.out.println("Amount of sentences to add");
                    int sentenceAmount = integerIn.nextInt();
                    String[] toAdd = new String[sentenceAmount];
                    System.out.println("Enter sentences. [Confirm by ENTER]:");
                    for (int i = 0; i < sentenceAmount; i++) toAdd[i] = stringIn.nextLine();
                    container = new AContainerMod(toAdd);
                }
                case 2 -> container.output();
                case 3 -> {
                    System.out.println("Enter data:");
                    container.add(stringIn.nextLine());
                    System.out.println();
                    container.output();
                }
                case 4 -> {
                    System.out.println("Element to delete:");
                    container.remove(stringIn.nextLine());
                    container.remove(stringIn.nextLine());
                }
                case 5 -> {
                    container.clear();
                    System.out.println("Cleared! \n");
                }
                case 6 -> {
                    System.out.println("Ascending order [1]");
                    System.out.println("Descending order [2]");
                    int choice = integerIn.nextInt();
                    if (choice == 1 || choice == 2) {
                        container.alphabetSorting(choice);
                    } else
                        System.out.println("Wrong operation");
                }
                case 7 -> {
                    System.out.println("Element to search:");
                    int position = container.search(stringIn.nextLine());
                    if (position != -1)
                        System.out.println("Your choice: " + (position + 1) + "\n");
                    else
                        System.out.println("There is no such element\n");
                }
                case 8 -> {
                    System.out.println("Your container: ");
                    container.output();
                    System.out.println("Enter positions of elements (from 1 to " + container.size() + "):");
                    int position1 = integerIn.nextInt();
                    int position2 = integerIn.nextInt();
                    boolean comp = container.compareElements(position1, position2);
                    if (comp)
                        System.out.println("Elements [" + position1 + "] and [" + position2 + "] are equal\n");
                    else
                        System.out.println("Elements [" + position1 + "] and [" + position2 + "] are not equal\n");
                }
                case 9 -> {
                    String text = SameFirstLast.getText();
                    SameFirstLast.findWords(text);
                }
                case 10 -> {
                    StringBuilder sentences = new StringBuilder(container.toString());
                    TextProcessor.sentenceFinder(sentences);
                }
                case 11 -> {
                    System.out.println("Serialization...");
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("S:\\University\\JavaTasksKhPI\\javalab"
                            + "\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin06\\SerializeFile.txt"));
                    oos.writeObject(container);
                    oos.close();
                    System.out.println("Serialization completed\n");
                }
                case 12 -> {
                    System.out.println("Deserialization...");
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("S:\\University\\JavaTasksKhPI\\javalab"
                            + "\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin06\\SerializeFile.txt"));
                    AContainerMod temp = (AContainerMod) ois.readObject();
                    ois.close();
                    System.out.println("Deserialization completed\n");
                    temp.output();
                }
                case 13 -> {
                    container.clear();
                    integerIn.close();
                    stringIn.close();
                    continueWorking = false;
                }
                default -> System.out.println("Choose right variant\n");
            }
        }
    }

}
