package ua.khpi.oop.EventContainer;

import ua.khpi.oop.matushkin07.EventScheduler;

import java.io.*;
import java.util.Iterator;

public class EventContainer<E> implements Iterable, Serializable {
    /**
     * Array of objects
     */
    public static EventScheduler[] events;

    /**
     * Constructor with no parameters
     */
    public EventContainer() {
        events = new EventScheduler[0];
    }

    /**
     * Constructor with parameter
     *
     * @param event event to add
     */
    public EventContainer(EventScheduler event) {
        events = new EventScheduler[1];
        events[0] = event;
    }

    /**
     * Method prints container
     */
    public static void printInfo() {
        for (int i = 0; i < events.length; i++) {
            System.out.format("%n����������� %d:%n", i + 1);
            events[i].printInfo();
        }
    }

    /**
     * For Iterable
     *
     * @return null
     */
    @Override
    public Iterator iterator() {
        return null;
    }

    /**
     * Method adds an object to container
     *
     * @param event
     * @return true if added successfully
     * @return false if failed
     */
    public boolean add(EventScheduler event) {
        try {
            EventScheduler[] temp = events;
            events = new EventScheduler[temp.length + 1];
            System.arraycopy(temp, 0, events, 0, temp.length);
            events[events.length - 1] = event;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean delete(int choise) {
        if (choise > -1 && choise <= events.length) {
            try {
                EventScheduler[] temp = events;
                events = new EventScheduler[temp.length - 1];
                System.arraycopy(temp, 0, events, 0, choise);
                int elemToDestinate = temp.length - choise - 1; //�������, � ������� ����� �������� ���������� ����� �����������
                System.arraycopy(temp, choise + 1, events, choise, elemToDestinate);
                return true;
            } catch (ClassCastException ex) {
                System.err.println("TUT ClassCastException");
            }
        }

        return false;
    }

    /**
     * Search the position of same object as in parameter
     *
     * @param event Event to find
     * @return position of element in container
     * @return -1 if object doesn't exist
     */
    public int search(EventScheduler event) {
        int position = 0;
        for (int i = 0; i < events.length; i++) {
            if (!events[i].getDate().equals(event.getDate())) {
                position++;
                continue;
            }
            if (!events[i].getTime().equals(event.getTime())) {
                position++;
                continue;
            }
            if (events[i].getDuration() != (event.getDuration())) {
                position++;
                continue;
            }
            if (!(events[i].getPlace().equals(event.getPlace()))) {
                position++;
                continue;
            }
            if (!(events[i].getDescription().equals(event.getDescription()))) {
                position++;
                continue;
            }
            System.out.println("part");
            String[] c = events[i].getParticipants();
            String[] v = event.getParticipants();
            if (c.length == v.length) {
                for (int j = 0; j < c.length; j++) {
                    System.out.println(c[j] + " " + v[j]);
                    if (!c[j].equals(v[j])) {
                        System.out.println("zalupa");
                        break;
                    }
                }
                return position + 1;
            } else position++;
        }
        return -1;
    }

    /**
     * Standard serialization
     *
     * @throws IOException
     */
    public void standartSer() throws IOException {
        System.out.println("Serialization...");
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("S:\\JavaTasksKhPI\\javalab\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin09\\lab09.txt"));
        for (EventScheduler e : events) {
            oos.writeObject(e);
        }
//        oos.writeObject(events);
        oos.close();
        System.out.println("Serialization completed\n");
    }

    /**
     * Standard deserialization
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void standartDeser() throws IOException, ClassNotFoundException {
        System.out.println("Deserialization...");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("S:\\JavaTasksKhPI\\javalab\\matushkin-oleksii\\src\\ua\\khpi\\oop\\matushkin09\\lab09.txt"));
        EventScheduler[] temp = (EventScheduler[]) ois.readObject();
        ois.close();
        System.out.println("Deserialization completed\n");
    }


    /**
     * Returns number of objects in container
     *
     * @return size of container
     */
    public int getsize() {
        if (events.length == 0)
            return 0;
        else return events.length;
    }

    public void clear() {
        events = null;
    }
}

