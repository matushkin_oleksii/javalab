package ua.khpi.oop.matushkin01;

import java.util.Arrays;
/**
 * @author Матушкін Олексій, КІТ-120б
 * @version 1.0
 */
public class Main {

    /**
     * <p>Головний метод, що викликає дочірні, для виконання обчислень</p>
     * Головний метод, що викликає дочірні, для виконання обчислень
     * @param args аргументи командного рядка
     */
    public static void main(String[] args) {
        First_task();
        Second_task();
        Third_task();
    }

    /**
     * Метод з реацлізацією індивідульного завдання №1
     *
     * Завдання:
     * Обрати тип змінних та встановити за допомогою констант та літералів початкові значення:
     * 1) число, що відповідає номеру залікової книжки за допомогою шістнадцяткового літералу;
     * 2) число, що відповідає номеру мобільного телефона (починаючи з 380...) за допомогою десяткового літералу;
     * 3) число, яке складається з останніх двох ненульових цифр номера мобільного телефону за допомогою двійкового літералу;
     * 4) число, яке складається з останніх чотирьох ненульових цифр номера мобільного телефону за допомогою вісімкового літералу;
     * 5) визначити збільшене на одиницю значення залишку від ділення на 26 зменшеного на одиницю номера студента в журналі групи;
     * 6) символ англійського алфавіту в верхньому регістрі, номер якого відповідає знайденому раніше значенню.
     */
    private static void First_task(){
        int Zachet = 0x154D; //число, що відповідає номеру залікової книжки за допомогою шістнадцяткового літералу;
        long Phone_number_d = 380501234567L; //число, що відповідає номеру мобільного телефона (починаючи з 380...) за допомогою десяткового літералу;
        int Phone_number_bi = 0b1000011; //число, яке складається з останніх двох ненульових цифр номера мобільного телефону за допомогою двійкового літералу;
        int Phone_number_oct = 010727; //число, яке складається з останніх чотирьох ненульових цифр номера мобільного телефону за допомогою вісімкового літералу;
        int _groupNum = ((13 - 1) % 26) + 1; //визначити збільшене на одиницю значення залишку від ділення на 26 зменшеного на одиницю номера студента в журналі групи;
        int Remainder = ((_groupNum - 1) % 26) - 1;
        char[] Alphabet = new char[26];
        for(int i = 0; i < 26; i++){
            Alphabet[i] = (char)(65 + i);
        }
        char Letter = Alphabet[Remainder]; //символ англійського алфавіту в верхньому регістрі, номер якого відповідає знайденому раніше значенню.
        System.out.println("Task 1");
        System.out.println("Hexadecimal number: " + Zachet);
        System.out.println("Decimal number: " + Phone_number_d);
        System.out.println("Binary number: " + Phone_number_bi);
        System.out.println("Octal number: " + Phone_number_oct);
        System.out.println("Number of letter in alphabet: " + Remainder);
        System.out.println("Letter: " + Letter);
    }

    /**
     * Метод з реацлізацією індивідульного завдання №2
     * Завдання:
     * Використовуючи десятковий запис цілочисельного значення кожної змінної знайти і підрахувати кількість парних і непарних цифр.
     */
    private static void Second_task(){
        int Number = 1353647827;
        String string = String.valueOf(Number);
        char[] array = string.toCharArray();
        int even = 0;
        int odd = 0;
        for (char c : array) {
            if ((int) c % 2 == 0) even++;
            else odd++;
        }
        System.out.println("Task 2");
        System.out.println("Amount of odd numbers: " + odd);
        System.out.println("Amount of odd even: " + even);
    }

    /**
     * Метод з реацлізацією індивідульного завдання №3
     * Завдання:
     * Використовуючи двійковий запис цілочисельного значення кожної змінної підрахувати кількість одиниць.
     */
    private static void Third_task(){
        int Number = 654456;
        String string = Integer.toBinaryString(Number);
        char[] array = string.toCharArray();
        int counter = 0;
        for (char c : array) {
            if (c == '1') counter++;
        }
        System.out.println("Task 3");
        System.out.println("Number in binary: " + Arrays.toString(array));
        System.out.println(counter);

    }
}
