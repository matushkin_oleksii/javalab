package ua.khpi.oop.matushkin07;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventScheduler implements Serializable {

    /**
     * ���� �����������
     */
    private String date;
    /**
     * ����� �����������
     */
    private String time;
    /**
     * ������������ �����������
     */
    private int duration;
    /**
     * ����� ����������
     */
    private String place;
    /**
     * ��������
     */
    private String description;
    /**
     * ���������
     */
    private String[] participants;

    /**
     * ��������� �������
     * @return ��������� ������
     * @throws ParseException ��� ������������ �������� ����
     * @throws IOException ��� ������������ ����������
     * @param b
     */
    public static EventScheduler generate(boolean b) throws ParseException, IOException {
        Scanner in = new Scanner(System.in);
        EventScheduler event = new EventScheduler();
        System.out.print("������� ���� ����������� (��.��.����): ");
        event.setDate(in.nextLine());
        System.out.print("������� ����� ������ ����������� (��:��): ");
        event.setTime(in.nextLine());
        System.out.print("������� ������������ ����������� (� �������): ");
        event.setDuration(in.nextInt());
        in.nextLine();
        System.out.print("������� ����� ����������: ");
        event.setPlace(in.nextLine());
        System.out.print("������� �������� �����������: ");
        event.setDescription(in.nextLine());
        System.out.print("������� ���������� ����������: ");
        int amount = in.nextInt();
        in.nextLine();
        event.setParticipants(amount);
        return event;
    }

    /**
     * ��������� ����.
     *
     * @return ����
     */
    public String getDate() {
        return this.date;
    }

    /**
     * ��������� ����.
     *
     * @param d - ����, ������� ����� ����������
     * @throws ParseException - ���� �� �������
     *                        �������� ����
     */
    public void setDate(final String d) throws ParseException {
            this.date = d;
    }


    /**
     * ��������� �������.
     *
     * @return �����
     */
    public String getTime() {
        return this.time;
    }

    /**
     * ��������� �������.
     *
     * @param t - �����, ������� ����� ����������
     * @throws ParseException - ���� �� �������
     *                        �������� �����
     */
    public void setTime(String t) throws ParseException {
            this.time = t;
    }

    /**
     * �������� �������.
     *
     * @param t - �����
     * @return - true, ���� ����� �������� �������
     */
    private boolean checkTime(String t) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            sdf.setLenient(false);
            sdf.parse(t);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        if (this.checkPlace(place)) {
            this.place = place;
        }
        this.place = place;
    }

    public boolean checkPlace(final String venue) {
        Pattern pattern = Pattern.compile("^[�-��-���]+(\\s[�-��-���]+)?");
        Matcher matcher = pattern.matcher(venue);
        return matcher.matches();
    }

    public String getDescription() {
        return this.description;
    }

    /**
     * ��������� �������� �����������.
     *
     * @param desc - ��������
     */
    public void setDescription(final String desc) {
        if (this.checkDesc(desc)) {
            this.description = desc;
        }
        this.description = desc;
    }

    /**
     * �������� �� ������������ ����� ��������.
     *
     * @param desc - ��������
     * @return true, ���� ������
     * ������������� ����������� ���������
     */
    private boolean checkDesc(final String desc) {
        Pattern pattern = Pattern.compile("^[�-��-���]+(\\s?[�-��-���]+)+\\.$");
        Matcher matcher = pattern.matcher(desc);
        return matcher.matches();
    }

    /**
     * ��������� ���������� �����������.
     *
     * @return ������ ����������
     */
    public String[] getParticipants() {
        return this.participants;
    }

    /**
     * ��������� ���������� �����������.
     *
     * @param partAmount - ���������� ����������
     * @throws IOException - ��� ������������ ����������
     */
    public void setParticipants(final int partAmount) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        this.participants = new String[partAmount];
        System.out.format("������� ����� %s ����������.%n", partAmount);
        String name;
        for (int i = 0; i < partAmount; i++) {
            System.out.format("�������� #%d: ", i + 1);
            name = reader.readLine();
            if (checkName(name)) {
                this.participants[i] = name;
            } else {
                i--;
            }
        }
    }

    /**
     * �������� �� ������������ ����� ����� ���������.
     *
     * @param name - ���
     * @return true, ���� ������
     * ������������� ����������� ���������
     */
    private boolean checkName(final String name) {
        Pattern pattern = Pattern.compile("^[�-��-���]+(\\s[�-��-���]+)?");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /**
     * ����� ���������� � �����������.
     */
    public void printInfo() {
        System.out.println("����: " + this.getDate());
        System.out.println("����� ������: " + this.getTime());
        System.out.println("������������: " + this.getDuration() + " �����");
        System.out.println("����� ����������: " + this.getPlace());
        System.out.println("��������: " + this.getDescription());
        System.out.print("���������: ");
        for (String name : this.getParticipants()) {
            System.out.print(name + " ");
        }
    }


}
