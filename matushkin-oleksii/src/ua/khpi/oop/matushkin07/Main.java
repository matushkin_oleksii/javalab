package ua.khpi.oop.matushkin07;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class Main {
        public static void main(final String[] args)
            throws IOException, ParseException {
            Scanner in = new Scanner(System.in);
            System.out.print("Сколько мероприятий добавить? ");
            int size = in.nextInt();
            in.nextLine();
            EventScheduler[] events = new EventScheduler[size];
            for (int i = 0; i < events.length; i++) {
                System.out.format("Мероприятие %d:%n", i + 1);
                events[i] = EventScheduler.generate(true);
            }
            System.out.println();
            for (int i = 0; i < events.length; i++) {
                System.out.format("Мероприятие %d:%n", i + 1);
                events[i].printInfo();
                System.out.println();
            }
        }
}
