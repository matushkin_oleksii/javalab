package ua.khpi.oop.matushkin02;

/**
 * @author Матушкін Олексій, КІТ-120б
 * @version 1.0
 */

import java.util.*;

/**
 * Основний клас
 */
public class Main {

    /**
     * Головний метод, що викликає дочірні методи.
     * @param args аргументи командного рядка
     */
    public static void main(String[] args) {
        highestNumber();
    }

    /*
    int result = ((13 - 1) % 10) + 1;
    System.out.println("Number of task: " + result);
    */
    // Знайти найбільшу цифру в десятковому запису цілочисельного значення.

    /**
     * Метод є стартовим для виконання прикладного завдання.
     * Задає максимальне число доступне для генерації та викликає методи обробки
     */
    
    private static void highestNumber() {
        int maxNumber = 66456545; //number should be in range 0 < number < maxNumber
        int maxNumberLength = (int) (Math.log10(maxNumber) + 1);
        int attempts = 20;
        for(int iteration_counter = 0; iteration_counter < attempts; iteration_counter++) {
            int operationCounter = 1;
            outputFormatter(iteration_counter, operationCounter);
            operationCounter++;

            int Number = setNumber(maxNumber); // Generating random number: 0 < number < range
            int lengthOfNumber = (int) (Math.log10(Number) + 1); //Using logarithmic function, then rounding by using (int) and getting count of digits in number
            outputFormatter(Number, maxNumberLength, lengthOfNumber);
            operationCounter++;

            int highestNumber = findHighestNumber(lengthOfNumber, Number);
            outputFormatter(highestNumber, operationCounter);
        }
    }

    /**
     * Метод генерує псевдо випадкове число
     * @param maxNumber максимально дуступне число для генерації
     * @return згенероване число
     */
    private static int setNumber(int maxNumber){ //Method generates random number
        Random random = new Random(); //java.util class
        return random.nextInt(maxNumber); // Generating random number: 0 < number < maxNumber
    }

    /**
     * Метод пошуку найбільшої цифри у згенерованному числі
     * @param lengthOfNumber довжина числа, що передається
     * @param Number число, що підлягає обробці
     * @return найбільша цифра числа
     */
    private static int findHighestNumber(int lengthOfNumber, int Number){ //Method finds the highest digit in number

        int highestNumber = 0;
        for (int j = 0; j < lengthOfNumber; j++) {
            int temp = Number % 10;
            if (temp >= highestNumber) highestNumber = temp;
            Number = Number / 10;
        }
        return highestNumber;
    }

    /**
     * Метод виведення даних про поточну ітерацію та результат обчислень на екран
     * @param parameter вказує на інформацію, що має виводитись: число або найблішьша цифра
     * @param operationCounter вказує на функцію, що має виконуватись
     */
    private static void outputFormatter(int parameter, int operationCounter){ //Method formats output information
        Formatter f = new Formatter(); //java.util class

        if (operationCounter == 1) {
            System.out.format("\nAttempt #" + (parameter + 1) + "\t\t\t");
        }
        else if(operationCounter == 3){
            System.out.format("Highest number is: %d", parameter);
        }
    }

    /**
     * Метод виведення данних про згенероване число на екран
     * @param parameter число, що має виводитись
     * @param maxNumberLength довжина максимально доступного числа
     * @param currentNumberLength довжина оброблюємого числа
     */
    private static void outputFormatter(int parameter, int maxNumberLength, int currentNumberLength){ //Method formats output information
        if (maxNumberLength - currentNumberLength >= 3)
            System.out.format("Generated number is: %d   \t\t\t", parameter);
        else if (maxNumberLength - currentNumberLength >= 2)
            System.out.format("Generated number is: %d  \t\t\t", parameter);
        else if (maxNumberLength - currentNumberLength >= 1)
            System.out.format("Generated number is: %d \t\t\t", parameter);
        else if (maxNumberLength - currentNumberLength == 0)
            System.out.format("Generated number is: %d\t\t\t", parameter);
    }
}

