package ua.khpi.oop.matushkin05;

/**
 * Class demonstrates methods from {@link AContainer}
 */
public class Program {

    public static void main(String[] args) {
        AContainer container1 = new AContainer();
        String string1 = new String();
        //Container's methods
        container1.add("Assembly");
        container1.add("is");
        container1.add("too");
        container1.add("*yourWord*");
        container1.add("language");

        System.out.println("\nmethod size()");
        System.out.println("Size: " + container1.size());

        System.out.println("\nmethod toString()");
        System.out.println("toString: " + container1.toString());

        String[] s1 = container1.toArray();
        System.out.println("\nmethod toArray()");
        for (String s : s1) {
            System.out.println(s);
        }

        System.out.println("\nmethod contains()");
        System.out.println(container1.contains("Big"));

        System.out.println("\nmethod containsAll()");
        AContainer container2 = new AContainer();
        container2.add("Assembly");
        container2.add("is");
        container2.add("too");
        container2.add("*yourWord*");
        container2.add("language");
        System.out.println("Should return true");
        System.out.println("Result: " + container1.containsAll(container2));

        System.out.println("\nmethod remove()");
        container1.remove("is");
        for (String s : container1) {
            System.out.println(s);
        }

        System.out.println("\nmethod clear()");
        container1.clear();
        for (String s : container1) {
            System.out.println(s);
        }

        // Using iterator's methods
        System.out.println("\nIterator's methods\n");
        AContainer.AIterator iterator = container2.iterator();
        for (String s : container2) {
            System.out.print(s + ' ');
        }
        System.out.println();
        if (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        iterator.remove();
        for (String s : container2) {
            System.out.print(s + ' ');
        }
        System.out.println();
        if (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        iterator.remove();
        for (String s : container2) {
            System.out.print(s + ' ');
        }
    }
}

